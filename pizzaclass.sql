-- phpMyAdmin SQL Dump
-- version 4.7.0
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1:3333
-- Generation Time: Jul 02, 2019 at 07:06 PM
-- Server version: 10.1.25-MariaDB
-- PHP Version: 5.6.31

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `pizzaclass`
--

-- --------------------------------------------------------

--
-- Table structure for table `customers`
--

CREATE TABLE `customers` (
  `id` int(11) NOT NULL,
  `name` varchar(50) NOT NULL,
  `email` varchar(250) NOT NULL,
  `password` varchar(100) NOT NULL,
  `address` varchar(250) NOT NULL,
  `phone` int(16) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `customers`
--

INSERT INTO `customers` (`id`, `name`, `email`, `password`, `address`, `phone`) VALUES
(6, 'rinal', 'rinalpatel235@gmail.com', 'Rinal123456', '123456,rue  manthet ', 514),
(7, 'Rinal', 'rinal@gmail.com', 'Rinal123', '123456,abc', 514),
(8, 'Jerry', 'jerry@jerry.com', 'Jerry12345', '123456,abc', 514);

-- --------------------------------------------------------

--
-- Table structure for table `orders`
--

CREATE TABLE `orders` (
  `id` int(11) NOT NULL,
  `pizzaId` int(11) NOT NULL,
  `name` int(11) NOT NULL,
  `email` varchar(50) NOT NULL,
  `shippingAddress` varchar(250) NOT NULL,
  `postalcode` varchar(10) NOT NULL,
  `contactNumber` int(16) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `pizzas`
--

CREATE TABLE `pizzas` (
  `id` int(11) NOT NULL,
  `customerId` int(11) NOT NULL,
  `size` enum('small','medium','large','x-large') NOT NULL,
  `crust` enum('thin','regular','stuffed') NOT NULL,
  `sauce` enum('pizza','bbq','nosauce') DEFAULT NULL,
  `pepperoni` enum('extrapepperoni','pepperoni','nopepperoni') DEFAULT NULL,
  `cheese` enum('extramozzarella','mozzarella','nomozarella') DEFAULT NULL,
  `mushrooms` enum('extramushrooms','mushrooms','nomushrooms') DEFAULT NULL,
  `greenPeppers` enum('extragreenPeppers','greenPeppers','nogreenPeppers') DEFAULT NULL,
  `bacon` enum('extrabacon','bacon','nobacon') DEFAULT NULL,
  `sausage` enum('extrasausage','sausage','nosausage') DEFAULT NULL,
  `ham` enum('extraham','ham','noham') DEFAULT NULL,
  `onions` enum('extraonions','onions','noonions') DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `pizzas`
--

INSERT INTO `pizzas` (`id`, `customerId`, `size`, `crust`, `sauce`, `pepperoni`, `cheese`, `mushrooms`, `greenPeppers`, `bacon`, `sausage`, `ham`, `onions`) VALUES
(6, 6, 'medium', 'regular', 'pizza', 'nopepperoni', 'mozzarella', 'nomushrooms', 'nogreenPeppers', 'nobacon', 'nosausage', 'noham', 'noonions'),
(7, 6, 'medium', 'regular', 'pizza', 'nopepperoni', 'mozzarella', 'nomushrooms', 'nogreenPeppers', 'nobacon', 'nosausage', 'noham', 'noonions'),
(8, 6, 'medium', 'regular', 'pizza', 'nopepperoni', 'mozzarella', 'nomushrooms', 'nogreenPeppers', 'nobacon', 'nosausage', 'noham', 'noonions'),
(9, 6, 'medium', 'regular', 'pizza', 'nopepperoni', 'mozzarella', 'nomushrooms', 'nogreenPeppers', 'nobacon', 'nosausage', 'noham', 'noonions'),
(10, 6, 'medium', 'regular', 'pizza', 'nopepperoni', 'mozzarella', 'nomushrooms', 'nogreenPeppers', 'nobacon', 'nosausage', 'noham', 'noonions'),
(11, 6, 'medium', 'regular', 'pizza', 'nopepperoni', 'mozzarella', 'nomushrooms', 'nogreenPeppers', 'nobacon', 'nosausage', 'noham', 'noonions'),
(12, 6, 'medium', 'regular', 'pizza', 'nopepperoni', 'mozzarella', 'nomushrooms', 'nogreenPeppers', 'nobacon', 'nosausage', 'noham', 'noonions'),
(13, 6, 'medium', 'regular', 'pizza', 'nopepperoni', 'mozzarella', 'nomushrooms', 'nogreenPeppers', 'nobacon', 'nosausage', 'noham', 'noonions'),
(14, 6, 'medium', 'regular', 'pizza', 'nopepperoni', 'mozzarella', 'nomushrooms', 'nogreenPeppers', 'nobacon', 'nosausage', 'noham', 'noonions'),
(15, 6, 'medium', 'regular', 'pizza', 'nopepperoni', 'mozzarella', 'mushrooms', 'nogreenPeppers', 'nobacon', 'nosausage', 'noham', 'noonions'),
(16, 6, 'medium', 'regular', 'pizza', 'nopepperoni', 'mozzarella', 'mushrooms', 'nogreenPeppers', 'nobacon', 'nosausage', 'noham', 'noonions'),
(17, 6, 'medium', 'regular', 'pizza', 'nopepperoni', 'mozzarella', 'mushrooms', 'nogreenPeppers', 'nobacon', 'nosausage', 'noham', 'noonions'),
(18, 6, 'medium', 'regular', 'pizza', 'nopepperoni', 'mozzarella', 'mushrooms', 'nogreenPeppers', 'nobacon', 'nosausage', 'noham', 'noonions'),
(19, 6, 'medium', 'regular', 'pizza', 'nopepperoni', 'mozzarella', 'mushrooms', 'nogreenPeppers', 'nobacon', 'nosausage', 'noham', 'noonions'),
(20, 7, 'medium', 'regular', 'pizza', 'nopepperoni', 'mozzarella', 'nomushrooms', 'nogreenPeppers', 'nobacon', 'nosausage', 'noham', 'noonions'),
(21, 7, 'medium', 'regular', 'pizza', 'nopepperoni', 'mozzarella', 'nomushrooms', 'nogreenPeppers', 'nobacon', 'nosausage', 'noham', 'noonions'),
(22, 8, 'medium', 'regular', 'pizza', 'nopepperoni', 'mozzarella', 'mushrooms', 'greenPeppers', 'nobacon', 'nosausage', 'noham', 'onions'),
(23, 8, 'medium', 'regular', 'pizza', 'nopepperoni', 'mozzarella', 'nomushrooms', 'greenPeppers', 'nobacon', 'nosausage', 'noham', 'onions');

-- --------------------------------------------------------

--
-- Table structure for table `reviews`
--

CREATE TABLE `reviews` (
  `id` int(11) NOT NULL,
  `customer_Id` int(11) NOT NULL,
  `name` varchar(50) NOT NULL,
  `email` varchar(250) NOT NULL,
  `comment` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `customers`
--
ALTER TABLE `customers`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `orders`
--
ALTER TABLE `orders`
  ADD PRIMARY KEY (`id`),
  ADD KEY `pizzaId` (`pizzaId`);

--
-- Indexes for table `pizzas`
--
ALTER TABLE `pizzas`
  ADD PRIMARY KEY (`id`),
  ADD KEY `customerId` (`customerId`);

--
-- Indexes for table `reviews`
--
ALTER TABLE `reviews`
  ADD PRIMARY KEY (`id`),
  ADD KEY `customer_Id` (`customer_Id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `customers`
--
ALTER TABLE `customers`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;
--
-- AUTO_INCREMENT for table `orders`
--
ALTER TABLE `orders`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `pizzas`
--
ALTER TABLE `pizzas`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=24;
--
-- AUTO_INCREMENT for table `reviews`
--
ALTER TABLE `reviews`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- Constraints for dumped tables
--

--
-- Constraints for table `orders`
--
ALTER TABLE `orders`
  ADD CONSTRAINT `orders_ibfk_1` FOREIGN KEY (`pizzaId`) REFERENCES `pizzas` (`id`);

--
-- Constraints for table `pizzas`
--
ALTER TABLE `pizzas`
  ADD CONSTRAINT `pizzas_ibfk_1` FOREIGN KEY (`customerId`) REFERENCES `customers` (`id`);

--
-- Constraints for table `reviews`
--
ALTER TABLE `reviews`
  ADD CONSTRAINT `reviews_ibfk_1` FOREIGN KEY (`customer_Id`) REFERENCES `customers` (`id`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
