<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* pizza_creation.html.twig */
class __TwigTemplate_73dae18ebc10fbdd83bb1a40ed08c2be153ef16b4e6630e1c34d8077ea146237 extends \Twig\Template
{
    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->blocks = [
            'content' => [$this, 'block_content'],
            'title' => [$this, 'block_title'],
        ];
    }

    protected function doGetParent(array $context)
    {
        // line 1
        return "master.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $this->parent = $this->loadTemplate("master.html.twig", "pizza_creation.html.twig", 1);
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 3
    public function block_content($context, array $blocks = [])
    {
        // line 4
        echo "    
    <div id=\"centerContent\">
        <form method=\"post\" id=\"form\">
            <h2>Create Your own</h2><br><br>
          <label> Size : </label>
        <div class=\"form-check form-check-inline\">
          <input type=\"radio\" class=\"form-check-input\" id=\"materialInline1\" name=\"rdSize\" ";
        // line 10
        echo twig_escape_filter($this->env, $this->getAttribute(($context["v"] ?? null), "small", []), "html", null, true);
        echo " value=\"small\" >
          <label class=\"form-check-label\" for=\"materialInline1\">Small</label>
        </div>

        <div class=\"form-check form-check-inline\">
          <input type=\"radio\" class=\"form-check-input\" id=\"materialInline2\" checked=\"true\" name=\"rdSize\" ";
        // line 15
        echo twig_escape_filter($this->env, $this->getAttribute(($context["v"] ?? null), "medium", []), "html", null, true);
        echo " value=\"medium\">
          <label class=\"form-check-label\" for=\"materialInline2\">Medium</label>
        </div>
        
        <div class=\"form-check form-check-inline\">
          <input type=\"radio\" class=\"form-check-input\" id=\"materialInline2\" name=\"rdSize\" ";
        // line 20
        echo twig_escape_filter($this->env, $this->getAttribute(($context["v"] ?? null), "large", []), "html", null, true);
        echo " value=\"large\">
          <label class=\"form-check-label\" for=\"materialInline2\">Large</label>
        </div>

        <div class=\"form-check form-check-inline\">
          <input type=\"radio\" class=\"form-check-input\" id=\"materialInline3\" name=\"rdSize\" ";
        // line 25
        echo twig_escape_filter($this->env, $this->getAttribute(($context["v"] ?? null), "xlarge", []), "html", null, true);
        echo " value=\"X-large\">
          <label class=\"form-check-label\" for=\"materialInline3\">X-large</label>
        </div>
          <br><br>
<!--=======================================================================================================================================================-->
        <label> Crust : </label>
        <div class=\"form-check form-check-inline\">
          <input type=\"radio\" class=\"form-check-input\" id=\"materialInline1\" name=\"rdCrust\" ";
        // line 32
        echo twig_escape_filter($this->env, $this->getAttribute(($context["v"] ?? null), "thin", []), "html", null, true);
        echo " value=\"thin\" >
          <label class=\"form-check-label\" for=\"materialInline1\">Thin</label>
        </div>

        <div class=\"form-check form-check-inline\">
          <input type=\"radio\" class=\"form-check-input\" id=\"materialInline2\" checked=\"true\" name=\"rdCrust\" ";
        // line 37
        echo twig_escape_filter($this->env, $this->getAttribute(($context["v"] ?? null), "regular", []), "html", null, true);
        echo " value=\"regular\">
          <label class=\"form-check-label\" for=\"materialInline2\">Regular</label>
        </div>
        
        <div class=\"form-check form-check-inline\">
          <input type=\"radio\" class=\"form-check-input\" id=\"materialInline2\" name=\"rdCrust\" ";
        // line 42
        echo twig_escape_filter($this->env, $this->getAttribute(($context["v"] ?? null), "stuffed", []), "html", null, true);
        echo " value=\"stuffed\">
          <label class=\"form-check-label\" for=\"materialInline2\">Stuffed</label>
        </div>
          <br><br>
          
<!--=======================================================================================================================================================-->

  <label> Sauce : </label>
        <div class=\"form-check form-check-inline\">
          <input type=\"radio\" class=\"form-check-input\" id=\"materialInline1\" checked=\"true\" name=\"rdsauce\" ";
        // line 51
        echo twig_escape_filter($this->env, $this->getAttribute(($context["v"] ?? null), "pizza", []), "html", null, true);
        echo " value=\"pizza\" >
          <label class=\"form-check-label\" for=\"materialInline1\">Pizza</label>
        </div>

        <div class=\"form-check form-check-inline\">
          <input type=\"radio\" class=\"form-check-input\" id=\"materialInline2\" name=\"rdsauce\" ";
        // line 56
        echo twig_escape_filter($this->env, $this->getAttribute(($context["v"] ?? null), "bbq", []), "html", null, true);
        echo " value=\"bbq\">
          <label class=\"form-check-label\" for=\"materialInline2\">BBQ</label>
        </div>
        
        <div class=\"form-check form-check-inline\">
          <input type=\"radio\" class=\"form-check-input\" id=\"materialInline2\" name=\"rdsauce\" ";
        // line 61
        echo twig_escape_filter($this->env, $this->getAttribute(($context["v"] ?? null), "nosauce", []), "html", null, true);
        echo " value=\"nosauce\">
          <label class=\"form-check-label\" for=\"materialInline2\">No sauce</label>
        </div>
           <br><br>
<!--=======================================================================================================================================================-->
 <label> Pepperoni : </label>
        <div class=\"form-check form-check-inline\">
          <input type=\"radio\" class=\"form-check-input\" id=\"materialInline1\" name=\"rdpepperoni\" ";
        // line 68
        echo twig_escape_filter($this->env, $this->getAttribute(($context["v"] ?? null), "extrapepperoni", []), "html", null, true);
        echo " value=\"extragreenPeppers\" >
          <label class=\"form-check-label\" for=\"materialInline1\">2X Pepperoni</label>
        </div>
          
        <div class=\"form-check form-check-inline\">
          <input type=\"radio\" class=\"form-check-input\" id=\"materialInline2\" name=\"rdpepperoni\" ";
        // line 73
        echo twig_escape_filter($this->env, $this->getAttribute(($context["v"] ?? null), "pepperoni", []), "html", null, true);
        echo " value=\"pepperoni\">
          <label class=\"form-check-label\" for=\"materialInline2\">pepperoni</label>
        </div>
        
        <div class=\"form-check form-check-inline\">
          <input type=\"radio\" class=\"form-check-input\" id=\"materialInline2\" checked=\"true\" checked=\"true\" name=\"rdpepperoni\" ";
        // line 78
        echo twig_escape_filter($this->env, $this->getAttribute(($context["v"] ?? null), "nopepperoni", []), "html", null, true);
        echo " value=\"nopepperoni\">
          <label class=\"form-check-label\" for=\"materialInline2\">No Pepperoni</label>
        </div>
          <br><br>         
<!--=======================================================================================================================================================-->

  <label> Cheese : </label>
        <div class=\"form-check form-check-inline\">
          <input type=\"radio\" class=\"form-check-input\" id=\"materialInline1\" name=\"rdcheese\" ";
        // line 86
        echo twig_escape_filter($this->env, $this->getAttribute(($context["v"] ?? null), "extramozzarella", []), "html", null, true);
        echo " value=\"extramozzarella\" >
          <label class=\"form-check-label\" for=\"materialInline1\">2X Mozzarella</label>
        </div>

        <div class=\"form-check form-check-inline\">
          <input type=\"radio\" class=\"form-check-input\" id=\"materialInline2\" checked=\"true\" name=\"rdcheese\" ";
        // line 91
        echo twig_escape_filter($this->env, $this->getAttribute(($context["v"] ?? null), "mozzarella", []), "html", null, true);
        echo " value=\"mozzarella\">
          <label class=\"form-check-label\" for=\"materialInline2\">Mozzarella</label>
        </div>
        
        <div class=\"form-check form-check-inline\">
          <input type=\"radio\" class=\"form-check-input\" id=\"materialInline2\" name=\"rdcheese\" ";
        // line 96
        echo twig_escape_filter($this->env, $this->getAttribute(($context["v"] ?? null), "nomozzarella", []), "html", null, true);
        echo " value=\"nomozzarella\">
          <label class=\"form-check-label\" for=\"materialInline2\">No mozzarella</label>
        </div>
           <br><br>
<!--=======================================================================================================================================================-->

  <label> Mushrooms : </label>
        <div class=\"form-check form-check-inline\">
          <input type=\"radio\" class=\"form-check-input\" id=\"materialInline1\" name=\"rdmushrooms\" ";
        // line 104
        echo twig_escape_filter($this->env, $this->getAttribute(($context["v"] ?? null), "extramushrooms", []), "html", null, true);
        echo " value=\"extramushrooms\" >
          <label class=\"form-check-label\" for=\"materialInline1\">2X Mushrooms</label>
        </div>
          
        <div class=\"form-check form-check-inline\">
          <input type=\"radio\" class=\"form-check-input\" id=\"materialInline2\" name=\"rdmushrooms\" ";
        // line 109
        echo twig_escape_filter($this->env, $this->getAttribute(($context["v"] ?? null), "mushrooms", []), "html", null, true);
        echo " value=\"mushrooms\">
          <label class=\"form-check-label\" for=\"materialInline2\">Mushrooms</label>
        </div>
        
        <div class=\"form-check form-check-inline\">
          <input type=\"radio\" class=\"form-check-input\" id=\"materialInline2\" checked=\"true\" name=\"rdmushrooms\" ";
        // line 114
        echo twig_escape_filter($this->env, $this->getAttribute(($context["v"] ?? null), "nomushrooms", []), "html", null, true);
        echo " value=\"nomushrooms\">
          <label class=\"form-check-label\" for=\"materialInline2\">No Mushrooms</label>
        </div>
           <br><br>
<!--=======================================================================================================================================================-->
 <label> GreenPeppers : </label>
        <div class=\"form-check form-check-inline\">
          <input type=\"radio\" class=\"form-check-input\" id=\"materialInline1\" name=\"rdgreenPeppers\" ";
        // line 121
        echo twig_escape_filter($this->env, $this->getAttribute(($context["v"] ?? null), "extragreenPeppers", []), "html", null, true);
        echo " value=\"extragreenPeppers\" >
          <label class=\"form-check-label\" for=\"materialInline1\">2X GreenPeppers</label>
        </div>
          
        <div class=\"form-check form-check-inline\">
          <input type=\"radio\" class=\"form-check-input\" id=\"materialInline2\" name=\"rdgreenPeppers\" ";
        // line 126
        echo twig_escape_filter($this->env, $this->getAttribute(($context["v"] ?? null), "greenPeppers", []), "html", null, true);
        echo " value=\"greenPeppers\">
          <label class=\"form-check-label\" for=\"materialInline2\">GreenPeppers</label>
        </div>
        
        <div class=\"form-check form-check-inline\">
          <input type=\"radio\" class=\"form-check-input\" id=\"materialInline2\" checked=\"true\" name=\"rdgreenPeppers\" ";
        // line 131
        echo twig_escape_filter($this->env, $this->getAttribute(($context["v"] ?? null), "nogreenPeppers", []), "html", null, true);
        echo " value=\"nogreenPeppers\">
          <label class=\"form-check-label\" for=\"materialInline2\">No GreenPeppers</label>
        </div>
           <br><br>
<!--=======================================================================================================================================================-->
<label> Bacon : </label>
        <div class=\"form-check form-check-inline\">
          <input type=\"radio\" class=\"form-check-input\" id=\"materialInline1\" name=\"rdbacon\" ";
        // line 138
        echo twig_escape_filter($this->env, $this->getAttribute(($context["v"] ?? null), "extrabacon", []), "html", null, true);
        echo " value=\"extrabacon\" >
          <label class=\"form-check-label\" for=\"materialInline1\">2X Bacon</label>
        </div>
          
        <div class=\"form-check form-check-inline\">
          <input type=\"radio\" class=\"form-check-input\" id=\"materialInline2\" name=\"rdbacon\" ";
        // line 143
        echo twig_escape_filter($this->env, $this->getAttribute(($context["v"] ?? null), "bacon", []), "html", null, true);
        echo " value=\"bacon\">
          <label class=\"form-check-label\" for=\"materialInline2\">Bacon</label>
        </div>
        
        <div class=\"form-check form-check-inline\">
          <input type=\"radio\" class=\"form-check-input\" id=\"materialInline2\" checked=\"true\" name=\"rdbacon\" ";
        // line 148
        echo twig_escape_filter($this->env, $this->getAttribute(($context["v"] ?? null), "nobacon", []), "html", null, true);
        echo " value=\"nobacon\">
          <label class=\"form-check-label\" for=\"materialInline2\">No bacon</label>
        </div>
           <br><br>
<!--=======================================================================================================================================================-->

 <label> Sausage : </label>
        <div class=\"form-check form-check-inline\">
          <input type=\"radio\" class=\"form-check-input\" id=\"materialInline1\" name=\"rdsausage\" ";
        // line 156
        echo twig_escape_filter($this->env, $this->getAttribute(($context["v"] ?? null), "extrasausage", []), "html", null, true);
        echo " value=\"extrasausage\" >
          <label class=\"form-check-label\" for=\"materialInline1\">2X Sausage</label>
        </div>
          
        <div class=\"form-check form-check-inline\">
          <input type=\"radio\" class=\"form-check-input\" id=\"materialInline2\" name=\"rdsausage\" ";
        // line 161
        echo twig_escape_filter($this->env, $this->getAttribute(($context["v"] ?? null), "sausage", []), "html", null, true);
        echo " value=\"sausage\">
          <label class=\"form-check-label\" for=\"materialInline2\">Sausage</label>
        </div>
        
        <div class=\"form-check form-check-inline\">
          <input type=\"radio\" class=\"form-check-input\" id=\"materialInline2\" checked=\"true\" name=\"rdsausage\" ";
        // line 166
        echo twig_escape_filter($this->env, $this->getAttribute(($context["v"] ?? null), "nosausage", []), "html", null, true);
        echo " value=\"nosausage\">
          <label class=\"form-check-label\" for=\"materialInline2\">No Sausage</label>
        </div>
           <br><br>
<!--=======================================================================================================================================================-->
  
 <label> Ham : </label>
        <div class=\"form-check form-check-inline\">
          <input type=\"radio\" class=\"form-check-input\" id=\"materialInline1\" name=\"rdham\" ";
        // line 174
        echo twig_escape_filter($this->env, $this->getAttribute(($context["v"] ?? null), "extraham", []), "html", null, true);
        echo " value=\"extraham\" >
          <label class=\"form-check-label\" for=\"materialInline1\">2X Ham</label>
        </div>
          
        <div class=\"form-check form-check-inline\">
          <input type=\"radio\" class=\"form-check-input\" id=\"materialInline2\" name=\"rdham\" ";
        // line 179
        echo twig_escape_filter($this->env, $this->getAttribute(($context["v"] ?? null), "ham", []), "html", null, true);
        echo " value=\"ham\">
          <label class=\"form-check-label\" for=\"materialInline2\">Ham</label>
        </div>
        
        <div class=\"form-check form-check-inline\">
          <input type=\"radio\" class=\"form-check-input\" id=\"materialInline2\" checked=\"true\" name=\"rdham\" ";
        // line 184
        echo twig_escape_filter($this->env, $this->getAttribute(($context["v"] ?? null), "noham", []), "html", null, true);
        echo " value=\"noham\">
          <label class=\"form-check-label\" for=\"materialInline2\">No Ham</label>
        </div>
           <br><br>
<!--=======================================================================================================================================================-->


 <label> Onions : </label>
        <div class=\"form-check form-check-inline\">
          <input type=\"radio\" class=\"form-check-input\" id=\"materialInline1\" name=\"rdonions\" ";
        // line 193
        echo twig_escape_filter($this->env, $this->getAttribute(($context["v"] ?? null), "extraonions", []), "html", null, true);
        echo " value=\"extraonions\" >
          <label class=\"form-check-label\" for=\"materialInline1\">2X Onions</label>
        </div>
          
        <div class=\"form-check form-check-inline\">
          <input type=\"radio\" class=\"form-check-input\" id=\"materialInline2\" name=\"rdonions\" ";
        // line 198
        echo twig_escape_filter($this->env, $this->getAttribute(($context["v"] ?? null), "onions", []), "html", null, true);
        echo " value=\"onions\">
          <label class=\"form-check-label\" for=\"materialInline2\">Onions</label>
        </div>
        
        <div class=\"form-check form-check-inline\">
          <input type=\"radio\" class=\"form-check-input\" id=\"materialInline2\" checked=\"true\" name=\"rdonions\" ";
        // line 203
        echo twig_escape_filter($this->env, $this->getAttribute(($context["v"] ?? null), "noonions", []), "html", null, true);
        echo " value=\"noonions\">
          <label class=\"form-check-label\" for=\"materialInline2\">No Onions</label>
        </div>
           <br><br>
<!--=======================================================================================================================================================-->
    <br><button type=\"submit\" value=\"confirm\"> Confirm An Order  </button>

    <br>
    <br>
   
 </div> 
</form>
</div>

";
    }

    // line 218
    public function block_title($context, array $blocks = [])
    {
        echo "Pizza Creation Page";
    }

    public function getTemplateName()
    {
        return "pizza_creation.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  362 => 218,  343 => 203,  335 => 198,  327 => 193,  315 => 184,  307 => 179,  299 => 174,  288 => 166,  280 => 161,  272 => 156,  261 => 148,  253 => 143,  245 => 138,  235 => 131,  227 => 126,  219 => 121,  209 => 114,  201 => 109,  193 => 104,  182 => 96,  174 => 91,  166 => 86,  155 => 78,  147 => 73,  139 => 68,  129 => 61,  121 => 56,  113 => 51,  101 => 42,  93 => 37,  85 => 32,  75 => 25,  67 => 20,  59 => 15,  51 => 10,  43 => 4,  40 => 3,  30 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Source("{% extends \"master.html.twig\" %}

{% block content %}
    
    <div id=\"centerContent\">
        <form method=\"post\" id=\"form\">
            <h2>Create Your own</h2><br><br>
          <label> Size : </label>
        <div class=\"form-check form-check-inline\">
          <input type=\"radio\" class=\"form-check-input\" id=\"materialInline1\" name=\"rdSize\" {{v.small}} value=\"small\" >
          <label class=\"form-check-label\" for=\"materialInline1\">Small</label>
        </div>

        <div class=\"form-check form-check-inline\">
          <input type=\"radio\" class=\"form-check-input\" id=\"materialInline2\" checked=\"true\" name=\"rdSize\" {{v.medium}} value=\"medium\">
          <label class=\"form-check-label\" for=\"materialInline2\">Medium</label>
        </div>
        
        <div class=\"form-check form-check-inline\">
          <input type=\"radio\" class=\"form-check-input\" id=\"materialInline2\" name=\"rdSize\" {{v.large}} value=\"large\">
          <label class=\"form-check-label\" for=\"materialInline2\">Large</label>
        </div>

        <div class=\"form-check form-check-inline\">
          <input type=\"radio\" class=\"form-check-input\" id=\"materialInline3\" name=\"rdSize\" {{v.xlarge}} value=\"X-large\">
          <label class=\"form-check-label\" for=\"materialInline3\">X-large</label>
        </div>
          <br><br>
<!--=======================================================================================================================================================-->
        <label> Crust : </label>
        <div class=\"form-check form-check-inline\">
          <input type=\"radio\" class=\"form-check-input\" id=\"materialInline1\" name=\"rdCrust\" {{v.thin}} value=\"thin\" >
          <label class=\"form-check-label\" for=\"materialInline1\">Thin</label>
        </div>

        <div class=\"form-check form-check-inline\">
          <input type=\"radio\" class=\"form-check-input\" id=\"materialInline2\" checked=\"true\" name=\"rdCrust\" {{v.regular}} value=\"regular\">
          <label class=\"form-check-label\" for=\"materialInline2\">Regular</label>
        </div>
        
        <div class=\"form-check form-check-inline\">
          <input type=\"radio\" class=\"form-check-input\" id=\"materialInline2\" name=\"rdCrust\" {{v.stuffed}} value=\"stuffed\">
          <label class=\"form-check-label\" for=\"materialInline2\">Stuffed</label>
        </div>
          <br><br>
          
<!--=======================================================================================================================================================-->

  <label> Sauce : </label>
        <div class=\"form-check form-check-inline\">
          <input type=\"radio\" class=\"form-check-input\" id=\"materialInline1\" checked=\"true\" name=\"rdsauce\" {{v.pizza}} value=\"pizza\" >
          <label class=\"form-check-label\" for=\"materialInline1\">Pizza</label>
        </div>

        <div class=\"form-check form-check-inline\">
          <input type=\"radio\" class=\"form-check-input\" id=\"materialInline2\" name=\"rdsauce\" {{v.bbq}} value=\"bbq\">
          <label class=\"form-check-label\" for=\"materialInline2\">BBQ</label>
        </div>
        
        <div class=\"form-check form-check-inline\">
          <input type=\"radio\" class=\"form-check-input\" id=\"materialInline2\" name=\"rdsauce\" {{v.nosauce}} value=\"nosauce\">
          <label class=\"form-check-label\" for=\"materialInline2\">No sauce</label>
        </div>
           <br><br>
<!--=======================================================================================================================================================-->
 <label> Pepperoni : </label>
        <div class=\"form-check form-check-inline\">
          <input type=\"radio\" class=\"form-check-input\" id=\"materialInline1\" name=\"rdpepperoni\" {{v.extrapepperoni}} value=\"extragreenPeppers\" >
          <label class=\"form-check-label\" for=\"materialInline1\">2X Pepperoni</label>
        </div>
          
        <div class=\"form-check form-check-inline\">
          <input type=\"radio\" class=\"form-check-input\" id=\"materialInline2\" name=\"rdpepperoni\" {{v.pepperoni}} value=\"pepperoni\">
          <label class=\"form-check-label\" for=\"materialInline2\">pepperoni</label>
        </div>
        
        <div class=\"form-check form-check-inline\">
          <input type=\"radio\" class=\"form-check-input\" id=\"materialInline2\" checked=\"true\" checked=\"true\" name=\"rdpepperoni\" {{v.nopepperoni}} value=\"nopepperoni\">
          <label class=\"form-check-label\" for=\"materialInline2\">No Pepperoni</label>
        </div>
          <br><br>         
<!--=======================================================================================================================================================-->

  <label> Cheese : </label>
        <div class=\"form-check form-check-inline\">
          <input type=\"radio\" class=\"form-check-input\" id=\"materialInline1\" name=\"rdcheese\" {{v.extramozzarella}} value=\"extramozzarella\" >
          <label class=\"form-check-label\" for=\"materialInline1\">2X Mozzarella</label>
        </div>

        <div class=\"form-check form-check-inline\">
          <input type=\"radio\" class=\"form-check-input\" id=\"materialInline2\" checked=\"true\" name=\"rdcheese\" {{v.mozzarella}} value=\"mozzarella\">
          <label class=\"form-check-label\" for=\"materialInline2\">Mozzarella</label>
        </div>
        
        <div class=\"form-check form-check-inline\">
          <input type=\"radio\" class=\"form-check-input\" id=\"materialInline2\" name=\"rdcheese\" {{v.nomozzarella}} value=\"nomozzarella\">
          <label class=\"form-check-label\" for=\"materialInline2\">No mozzarella</label>
        </div>
           <br><br>
<!--=======================================================================================================================================================-->

  <label> Mushrooms : </label>
        <div class=\"form-check form-check-inline\">
          <input type=\"radio\" class=\"form-check-input\" id=\"materialInline1\" name=\"rdmushrooms\" {{v.extramushrooms}} value=\"extramushrooms\" >
          <label class=\"form-check-label\" for=\"materialInline1\">2X Mushrooms</label>
        </div>
          
        <div class=\"form-check form-check-inline\">
          <input type=\"radio\" class=\"form-check-input\" id=\"materialInline2\" name=\"rdmushrooms\" {{v.mushrooms}} value=\"mushrooms\">
          <label class=\"form-check-label\" for=\"materialInline2\">Mushrooms</label>
        </div>
        
        <div class=\"form-check form-check-inline\">
          <input type=\"radio\" class=\"form-check-input\" id=\"materialInline2\" checked=\"true\" name=\"rdmushrooms\" {{v.nomushrooms}} value=\"nomushrooms\">
          <label class=\"form-check-label\" for=\"materialInline2\">No Mushrooms</label>
        </div>
           <br><br>
<!--=======================================================================================================================================================-->
 <label> GreenPeppers : </label>
        <div class=\"form-check form-check-inline\">
          <input type=\"radio\" class=\"form-check-input\" id=\"materialInline1\" name=\"rdgreenPeppers\" {{v.extragreenPeppers}} value=\"extragreenPeppers\" >
          <label class=\"form-check-label\" for=\"materialInline1\">2X GreenPeppers</label>
        </div>
          
        <div class=\"form-check form-check-inline\">
          <input type=\"radio\" class=\"form-check-input\" id=\"materialInline2\" name=\"rdgreenPeppers\" {{v.greenPeppers}} value=\"greenPeppers\">
          <label class=\"form-check-label\" for=\"materialInline2\">GreenPeppers</label>
        </div>
        
        <div class=\"form-check form-check-inline\">
          <input type=\"radio\" class=\"form-check-input\" id=\"materialInline2\" checked=\"true\" name=\"rdgreenPeppers\" {{v.nogreenPeppers}} value=\"nogreenPeppers\">
          <label class=\"form-check-label\" for=\"materialInline2\">No GreenPeppers</label>
        </div>
           <br><br>
<!--=======================================================================================================================================================-->
<label> Bacon : </label>
        <div class=\"form-check form-check-inline\">
          <input type=\"radio\" class=\"form-check-input\" id=\"materialInline1\" name=\"rdbacon\" {{v.extrabacon}} value=\"extrabacon\" >
          <label class=\"form-check-label\" for=\"materialInline1\">2X Bacon</label>
        </div>
          
        <div class=\"form-check form-check-inline\">
          <input type=\"radio\" class=\"form-check-input\" id=\"materialInline2\" name=\"rdbacon\" {{v.bacon}} value=\"bacon\">
          <label class=\"form-check-label\" for=\"materialInline2\">Bacon</label>
        </div>
        
        <div class=\"form-check form-check-inline\">
          <input type=\"radio\" class=\"form-check-input\" id=\"materialInline2\" checked=\"true\" name=\"rdbacon\" {{v.nobacon}} value=\"nobacon\">
          <label class=\"form-check-label\" for=\"materialInline2\">No bacon</label>
        </div>
           <br><br>
<!--=======================================================================================================================================================-->

 <label> Sausage : </label>
        <div class=\"form-check form-check-inline\">
          <input type=\"radio\" class=\"form-check-input\" id=\"materialInline1\" name=\"rdsausage\" {{v.extrasausage}} value=\"extrasausage\" >
          <label class=\"form-check-label\" for=\"materialInline1\">2X Sausage</label>
        </div>
          
        <div class=\"form-check form-check-inline\">
          <input type=\"radio\" class=\"form-check-input\" id=\"materialInline2\" name=\"rdsausage\" {{v.sausage}} value=\"sausage\">
          <label class=\"form-check-label\" for=\"materialInline2\">Sausage</label>
        </div>
        
        <div class=\"form-check form-check-inline\">
          <input type=\"radio\" class=\"form-check-input\" id=\"materialInline2\" checked=\"true\" name=\"rdsausage\" {{v.nosausage}} value=\"nosausage\">
          <label class=\"form-check-label\" for=\"materialInline2\">No Sausage</label>
        </div>
           <br><br>
<!--=======================================================================================================================================================-->
  
 <label> Ham : </label>
        <div class=\"form-check form-check-inline\">
          <input type=\"radio\" class=\"form-check-input\" id=\"materialInline1\" name=\"rdham\" {{v.extraham}} value=\"extraham\" >
          <label class=\"form-check-label\" for=\"materialInline1\">2X Ham</label>
        </div>
          
        <div class=\"form-check form-check-inline\">
          <input type=\"radio\" class=\"form-check-input\" id=\"materialInline2\" name=\"rdham\" {{v.ham}} value=\"ham\">
          <label class=\"form-check-label\" for=\"materialInline2\">Ham</label>
        </div>
        
        <div class=\"form-check form-check-inline\">
          <input type=\"radio\" class=\"form-check-input\" id=\"materialInline2\" checked=\"true\" name=\"rdham\" {{v.noham}} value=\"noham\">
          <label class=\"form-check-label\" for=\"materialInline2\">No Ham</label>
        </div>
           <br><br>
<!--=======================================================================================================================================================-->


 <label> Onions : </label>
        <div class=\"form-check form-check-inline\">
          <input type=\"radio\" class=\"form-check-input\" id=\"materialInline1\" name=\"rdonions\" {{v.extraonions}} value=\"extraonions\" >
          <label class=\"form-check-label\" for=\"materialInline1\">2X Onions</label>
        </div>
          
        <div class=\"form-check form-check-inline\">
          <input type=\"radio\" class=\"form-check-input\" id=\"materialInline2\" name=\"rdonions\" {{v.onions}} value=\"onions\">
          <label class=\"form-check-label\" for=\"materialInline2\">Onions</label>
        </div>
        
        <div class=\"form-check form-check-inline\">
          <input type=\"radio\" class=\"form-check-input\" id=\"materialInline2\" checked=\"true\" name=\"rdonions\" {{v.noonions}} value=\"noonions\">
          <label class=\"form-check-label\" for=\"materialInline2\">No Onions</label>
        </div>
           <br><br>
<!--=======================================================================================================================================================-->
    <br><button type=\"submit\" value=\"confirm\"> Confirm An Order  </button>

    <br>
    <br>
   
 </div> 
</form>
</div>

{% endblock %}
{% block title %}Pizza Creation Page{% endblock %}

", "pizza_creation.html.twig", "C:\\xampp\\htdocs\\ipd17-project\\templates\\pizza_creation.html.twig");
    }
}
