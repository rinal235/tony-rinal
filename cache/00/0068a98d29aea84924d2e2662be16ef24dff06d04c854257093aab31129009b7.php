<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* customer_review.html.twig */
class __TwigTemplate_22bced38bc8555eed841e1e7b1421b0432d37ef5950663123ccf47376b7bc594 extends \Twig\Template
{
    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->blocks = [
            'headAdd' => [$this, 'block_headAdd'],
            'title' => [$this, 'block_title'],
            'content' => [$this, 'block_content'],
        ];
    }

    protected function doGetParent(array $context)
    {
        // line 1
        return "master.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $this->parent = $this->loadTemplate("master.html.twig", "customer_review.html.twig", 1);
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 3
    public function block_headAdd($context, array $blocks = [])
    {
        // line 4
        echo " 
 ";
    }

    // line 7
    public function block_title($context, array $blocks = [])
    {
        echo "customer review ratings";
    }

    // line 9
    public function block_content($context, array $blocks = [])
    {
        // line 10
        echo "       <div id=\"centerContent\">
<form method=\"post\" id=\"form\" class=\"textbox\">
        <input class=\"textbox\" type=\"text\" name=\"name\" value=\"";
        // line 12
        echo twig_escape_filter($this->env, $this->getAttribute(($context["v"] ?? null), "name", []), "html", null, true);
        echo "\" placeholder=\"Name\"><br><br>
        <input class=\"textbox\" type=\"text\" name=\"name\" value=\"";
        // line 13
        echo twig_escape_filter($this->env, $this->getAttribute(($context["v"] ?? null), "email", []), "html", null, true);
        echo "\" placeholder=\"Email\"><br><br>
        <input class=\"textbox\" type=\"text\" name=\"name\" value=\"";
        // line 14
        echo twig_escape_filter($this->env, $this->getAttribute(($context["v"] ?? null), "comment", []), "html", null, true);
        echo "\" placeholder=\"Comment\"><br><br>
        <input class=\"textbox\" type=\"submit\" value=\"submit\">

    </form>
      </div>
    <!-- start   -->
    <section id=\"home-reviews\">
\t\t\t\t\t<div class=\"col-sm-10 ml-auto mr-auto text-center\">
\t\t\t\t\t\t<h2 class=\"\">Read what some of our customers are saying</h2>
\t\t\t\t\t\t<div id=\"carouselExampleControls\" class=\"carousel slide\" data-ride=\"carousel\">
\t\t\t\t\t\t\t<div class=\"carousel-inner\">
\t\t\t\t\t\t\t\t<div class=\"carousel-item active\">
\t\t\t\t\t\t\t\t\t<p class=\"review-body\"><i class=\"fa fa-quote-left\" aria-hidden=\"true\"></i>
\t\tWe appreciate the way in which the restaurant is run. Like the way they welcome us<i class=\"fa fa-quote-right\" aria-hidden=\"true\"></i></p>
\t\t\t\t\t\t\t\t\t<p class=\"review-name\">Valued Customer at Buddies Pizza.</p>
\t\t\t\t\t\t\t\t\t<div class=\"stars\">
\t\t\t\t\t\t\t\t\t\t<i class=\"fa fa-star fa-lg\" aria-hidden=\"true\"></i>
\t\t\t\t\t\t\t\t\t\t<i class=\"fa fa-star fa-lg\" aria-hidden=\"true\"></i>
\t\t\t\t\t\t\t\t\t\t<i class=\"fa fa-star fa-lg\" aria-hidden=\"true\"></i>
\t\t\t\t\t\t\t\t\t\t<i class=\"fa fa-star fa-lg\" aria-hidden=\"true\"></i>
\t\t\t\t\t\t\t\t\t\t<i class=\"fa fa-star-o fa-lg\" aria-hidden=\"true\"></i>
\t\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t<div class=\"carousel-item\">
\t\t\t\t\t\t\t\t\t<p class=\"review-body\"><i class=\"fa fa-quote-left\" aria-hidden=\"true\"></i>
\t\tI have always had a great experience eating here. <i class=\"fa fa-quote-right\" aria-hidden=\"true\"></i>
\t\t</p>
\t\t\t\t\t\t\t\t\t<p class=\"review-name\">Jeremy Customer at Buddies Pizza.</p>
\t\t\t\t\t\t\t\t\t<div class=\"stars\">
\t\t\t\t\t\t\t\t\t\t<i class=\"fa fa-star fa-lg\" aria-hidden=\"true\"></i>
\t\t\t\t\t\t\t\t\t\t<i class=\"fa fa-star fa-lg\" aria-hidden=\"true\"></i>
\t\t\t\t\t\t\t\t\t\t<i class=\"fa fa-star fa-lg\" aria-hidden=\"true\"></i>
\t\t\t\t\t\t\t\t\t\t<i class=\"fa fa-star fa-lg\" aria-hidden=\"true\"></i>
\t\t\t\t\t\t\t\t\t\t<i class=\"fa fa-star fa-lg\" aria-hidden=\"true\"></i>
\t\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t<div class=\"carousel-item\">
\t\t\t\t\t\t\t\t\t<p class=\"review-body\"><i class=\"fa fa-quote-left\" aria-hidden=\"true\"></i>
\t\tSimply amazing.<i class=\"fa fa-quote-right\" aria-hidden=\"true\"></i>
\t\t</p>
\t\t\t\t\t\t\t\t\t<p class=\"review-name\">Matthew at Enterprise Rent A Car</p>
\t\t\t\t\t\t\t\t\t<div class=\"stars\">
\t\t\t\t\t\t\t\t\t\t<i class=\"fa fa-star fa-lg\" aria-hidden=\"true\"></i>
\t\t\t\t\t\t\t\t\t\t<i class=\"fa fa-star fa-lg\" aria-hidden=\"true\"></i>
\t\t\t\t\t\t\t\t\t\t<i class=\"fa fa-star fa-lg\" aria-hidden=\"true\"></i>
\t\t\t\t\t\t\t\t\t\t<i class=\"fa fa-star fa-lg\" aria-hidden=\"true\"></i>
\t\t\t\t\t\t\t\t\t\t<i class=\"fa fa-star fa-lg\" aria-hidden=\"true\"></i>
\t\t\t\t\t\t\t\t\t</div>
\t\t
\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t<div class=\"carousel-item\">
\t\t\t\t\t\t\t\t\t<p class=\"review-body\"><i class=\"fa fa-quote-left\" aria-hidden=\"true\"></i>
\t\tYour pizza is outstanding.<i class=\"fa fa-quote-right\" aria-hidden=\"true\"></i>
\t\t</p>
\t\t\t\t\t\t\t\t\t<p class=\"review-name\">Ted at Fall River Enterprises, Inc.</p>
\t\t\t\t\t\t\t\t\t<div class=\"stars\">
\t\t\t\t\t\t\t\t\t\t<i class=\"fa fa-star fa-lg\" aria-hidden=\"true\"></i>
\t\t\t\t\t\t\t\t\t\t<i class=\"fa fa-star fa-lg\" aria-hidden=\"true\"></i>
\t\t\t\t\t\t\t\t\t\t<i class=\"fa fa-star fa-lg\" aria-hidden=\"true\"></i>
\t\t\t\t\t\t\t\t\t\t<i class=\"fa fa-star fa-lg\" aria-hidden=\"true\"></i>
\t\t\t\t\t\t\t\t\t\t<i class=\"fa fa-star-o fa-lg\" aria-hidden=\"true\"></i>
\t\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t<div class=\"carousel-item\">
\t\t\t\t\t\t\t\t\t<p class=\"review-body\"><i class=\"fa fa-quote-left\" aria-hidden=\"true\"></i>
\t\tvery professional and courteous.<i class=\"fa fa-quote-right\" aria-hidden=\"true\"></i>
\t\t</p>
\t\t\t\t\t\t\t\t\t<p class=\"review-name\">Valued Customer at FedEx Ground</p>
\t\t\t\t\t\t\t\t\t<div class=\"stars\">
\t\t\t\t\t\t\t\t\t\t<i class=\"fa fa-star fa-lg\" aria-hidden=\"true\"></i>
\t\t\t\t\t\t\t\t\t\t<i class=\"fa fa-star fa-lg\" aria-hidden=\"true\"></i>
\t\t\t\t\t\t\t\t\t\t<i class=\"fa fa-star fa-lg\" aria-hidden=\"true\"></i>
\t\t\t\t\t\t\t\t\t\t<i class=\"fa fa-star fa-lg\" aria-hidden=\"true\"></i>
\t\t\t\t\t\t\t\t\t\t<i class=\"fa fa-star fa-lg\" aria-hidden=\"true\"></i>
\t\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t</div>
\t\t\t\t\t</div>
\t\t\t\t</section>
    <!--  end -->
  <div id=\"centerContent\">
      <form method=\"post\" id=\"form\" class=\"review\">
        <span class=\"heading\">User Rating</span>
        <span class=\"fa fa-star checked\"></span>
        <span class=\"fa fa-star checked\"></span>
        <span class=\"fa fa-star checked\"></span>
        <span class=\"fa fa-star checked\"></span>
        <span class=\"fa fa-star\"></span>
        <p>4.1 average based on 254 reviews.</p>
        <hr style=\"border:3px solid #f1f1f1\">

        <div class=\"row\">
          <div class=\"side\">
            <div>5 star</div>
          </div>
          <div class=\"middle\">
            <div class=\"bar-container\">
              <div class=\"bar-5\"></div>
            </div>
          </div>
          <div class=\"side right\">
            <div>150</div>
          </div>
          <div class=\"side\">
            <div>4 star</div>
          </div>
          <div class=\"middle\">
            <div class=\"bar-container\">
              <div class=\"bar-4\"></div>
            </div>
          </div>
          <div class=\"side right\">
            <div>63</div>
          </div>
          <div class=\"side\">
            <div>3 star</div>
          </div>
          <div class=\"middle\">
            <div class=\"bar-container\">
              <div class=\"bar-3\"></div>
            </div>
          </div>
          <div class=\"side right\">
            <div>15</div>
          </div>
          <div class=\"side\">
            <div>2 star</div>
          </div>
          <div class=\"middle\">
            <div class=\"bar-container\">
              <div class=\"bar-2\"></div>
            </div>
          </div>
          <div class=\"side right\">
            <div>6</div>
          </div>
          <div class=\"side\">
            <div>1 star</div>
          </div>
          <div class=\"middle\">
            <div class=\"bar-container\">
              <div class=\"bar-1\"></div>
            </div>
          </div>
          <div class=\"side right\">
            <div>20</div>
          </div>
        </div>
   </form>
 </div>
    
  
";
    }

    public function getTemplateName()
    {
        return "customer_review.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  70 => 14,  66 => 13,  62 => 12,  58 => 10,  55 => 9,  49 => 7,  44 => 4,  41 => 3,  31 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Source("{% extends \"master.html.twig\" %}

{% block headAdd %}
 
 {% endblock %}
 
 {% block title %}customer review ratings{% endblock title %}
 
{% block content %}
       <div id=\"centerContent\">
<form method=\"post\" id=\"form\" class=\"textbox\">
        <input class=\"textbox\" type=\"text\" name=\"name\" value=\"{{v.name}}\" placeholder=\"Name\"><br><br>
        <input class=\"textbox\" type=\"text\" name=\"name\" value=\"{{v.email}}\" placeholder=\"Email\"><br><br>
        <input class=\"textbox\" type=\"text\" name=\"name\" value=\"{{v.comment}}\" placeholder=\"Comment\"><br><br>
        <input class=\"textbox\" type=\"submit\" value=\"submit\">

    </form>
      </div>
    <!-- start   -->
    <section id=\"home-reviews\">
\t\t\t\t\t<div class=\"col-sm-10 ml-auto mr-auto text-center\">
\t\t\t\t\t\t<h2 class=\"\">Read what some of our customers are saying</h2>
\t\t\t\t\t\t<div id=\"carouselExampleControls\" class=\"carousel slide\" data-ride=\"carousel\">
\t\t\t\t\t\t\t<div class=\"carousel-inner\">
\t\t\t\t\t\t\t\t<div class=\"carousel-item active\">
\t\t\t\t\t\t\t\t\t<p class=\"review-body\"><i class=\"fa fa-quote-left\" aria-hidden=\"true\"></i>
\t\tWe appreciate the way in which the restaurant is run. Like the way they welcome us<i class=\"fa fa-quote-right\" aria-hidden=\"true\"></i></p>
\t\t\t\t\t\t\t\t\t<p class=\"review-name\">Valued Customer at Buddies Pizza.</p>
\t\t\t\t\t\t\t\t\t<div class=\"stars\">
\t\t\t\t\t\t\t\t\t\t<i class=\"fa fa-star fa-lg\" aria-hidden=\"true\"></i>
\t\t\t\t\t\t\t\t\t\t<i class=\"fa fa-star fa-lg\" aria-hidden=\"true\"></i>
\t\t\t\t\t\t\t\t\t\t<i class=\"fa fa-star fa-lg\" aria-hidden=\"true\"></i>
\t\t\t\t\t\t\t\t\t\t<i class=\"fa fa-star fa-lg\" aria-hidden=\"true\"></i>
\t\t\t\t\t\t\t\t\t\t<i class=\"fa fa-star-o fa-lg\" aria-hidden=\"true\"></i>
\t\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t<div class=\"carousel-item\">
\t\t\t\t\t\t\t\t\t<p class=\"review-body\"><i class=\"fa fa-quote-left\" aria-hidden=\"true\"></i>
\t\tI have always had a great experience eating here. <i class=\"fa fa-quote-right\" aria-hidden=\"true\"></i>
\t\t</p>
\t\t\t\t\t\t\t\t\t<p class=\"review-name\">Jeremy Customer at Buddies Pizza.</p>
\t\t\t\t\t\t\t\t\t<div class=\"stars\">
\t\t\t\t\t\t\t\t\t\t<i class=\"fa fa-star fa-lg\" aria-hidden=\"true\"></i>
\t\t\t\t\t\t\t\t\t\t<i class=\"fa fa-star fa-lg\" aria-hidden=\"true\"></i>
\t\t\t\t\t\t\t\t\t\t<i class=\"fa fa-star fa-lg\" aria-hidden=\"true\"></i>
\t\t\t\t\t\t\t\t\t\t<i class=\"fa fa-star fa-lg\" aria-hidden=\"true\"></i>
\t\t\t\t\t\t\t\t\t\t<i class=\"fa fa-star fa-lg\" aria-hidden=\"true\"></i>
\t\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t<div class=\"carousel-item\">
\t\t\t\t\t\t\t\t\t<p class=\"review-body\"><i class=\"fa fa-quote-left\" aria-hidden=\"true\"></i>
\t\tSimply amazing.<i class=\"fa fa-quote-right\" aria-hidden=\"true\"></i>
\t\t</p>
\t\t\t\t\t\t\t\t\t<p class=\"review-name\">Matthew at Enterprise Rent A Car</p>
\t\t\t\t\t\t\t\t\t<div class=\"stars\">
\t\t\t\t\t\t\t\t\t\t<i class=\"fa fa-star fa-lg\" aria-hidden=\"true\"></i>
\t\t\t\t\t\t\t\t\t\t<i class=\"fa fa-star fa-lg\" aria-hidden=\"true\"></i>
\t\t\t\t\t\t\t\t\t\t<i class=\"fa fa-star fa-lg\" aria-hidden=\"true\"></i>
\t\t\t\t\t\t\t\t\t\t<i class=\"fa fa-star fa-lg\" aria-hidden=\"true\"></i>
\t\t\t\t\t\t\t\t\t\t<i class=\"fa fa-star fa-lg\" aria-hidden=\"true\"></i>
\t\t\t\t\t\t\t\t\t</div>
\t\t
\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t<div class=\"carousel-item\">
\t\t\t\t\t\t\t\t\t<p class=\"review-body\"><i class=\"fa fa-quote-left\" aria-hidden=\"true\"></i>
\t\tYour pizza is outstanding.<i class=\"fa fa-quote-right\" aria-hidden=\"true\"></i>
\t\t</p>
\t\t\t\t\t\t\t\t\t<p class=\"review-name\">Ted at Fall River Enterprises, Inc.</p>
\t\t\t\t\t\t\t\t\t<div class=\"stars\">
\t\t\t\t\t\t\t\t\t\t<i class=\"fa fa-star fa-lg\" aria-hidden=\"true\"></i>
\t\t\t\t\t\t\t\t\t\t<i class=\"fa fa-star fa-lg\" aria-hidden=\"true\"></i>
\t\t\t\t\t\t\t\t\t\t<i class=\"fa fa-star fa-lg\" aria-hidden=\"true\"></i>
\t\t\t\t\t\t\t\t\t\t<i class=\"fa fa-star fa-lg\" aria-hidden=\"true\"></i>
\t\t\t\t\t\t\t\t\t\t<i class=\"fa fa-star-o fa-lg\" aria-hidden=\"true\"></i>
\t\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t<div class=\"carousel-item\">
\t\t\t\t\t\t\t\t\t<p class=\"review-body\"><i class=\"fa fa-quote-left\" aria-hidden=\"true\"></i>
\t\tvery professional and courteous.<i class=\"fa fa-quote-right\" aria-hidden=\"true\"></i>
\t\t</p>
\t\t\t\t\t\t\t\t\t<p class=\"review-name\">Valued Customer at FedEx Ground</p>
\t\t\t\t\t\t\t\t\t<div class=\"stars\">
\t\t\t\t\t\t\t\t\t\t<i class=\"fa fa-star fa-lg\" aria-hidden=\"true\"></i>
\t\t\t\t\t\t\t\t\t\t<i class=\"fa fa-star fa-lg\" aria-hidden=\"true\"></i>
\t\t\t\t\t\t\t\t\t\t<i class=\"fa fa-star fa-lg\" aria-hidden=\"true\"></i>
\t\t\t\t\t\t\t\t\t\t<i class=\"fa fa-star fa-lg\" aria-hidden=\"true\"></i>
\t\t\t\t\t\t\t\t\t\t<i class=\"fa fa-star fa-lg\" aria-hidden=\"true\"></i>
\t\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t</div>
\t\t\t\t\t</div>
\t\t\t\t</section>
    <!--  end -->
  <div id=\"centerContent\">
      <form method=\"post\" id=\"form\" class=\"review\">
        <span class=\"heading\">User Rating</span>
        <span class=\"fa fa-star checked\"></span>
        <span class=\"fa fa-star checked\"></span>
        <span class=\"fa fa-star checked\"></span>
        <span class=\"fa fa-star checked\"></span>
        <span class=\"fa fa-star\"></span>
        <p>4.1 average based on 254 reviews.</p>
        <hr style=\"border:3px solid #f1f1f1\">

        <div class=\"row\">
          <div class=\"side\">
            <div>5 star</div>
          </div>
          <div class=\"middle\">
            <div class=\"bar-container\">
              <div class=\"bar-5\"></div>
            </div>
          </div>
          <div class=\"side right\">
            <div>150</div>
          </div>
          <div class=\"side\">
            <div>4 star</div>
          </div>
          <div class=\"middle\">
            <div class=\"bar-container\">
              <div class=\"bar-4\"></div>
            </div>
          </div>
          <div class=\"side right\">
            <div>63</div>
          </div>
          <div class=\"side\">
            <div>3 star</div>
          </div>
          <div class=\"middle\">
            <div class=\"bar-container\">
              <div class=\"bar-3\"></div>
            </div>
          </div>
          <div class=\"side right\">
            <div>15</div>
          </div>
          <div class=\"side\">
            <div>2 star</div>
          </div>
          <div class=\"middle\">
            <div class=\"bar-container\">
              <div class=\"bar-2\"></div>
            </div>
          </div>
          <div class=\"side right\">
            <div>6</div>
          </div>
          <div class=\"side\">
            <div>1 star</div>
          </div>
          <div class=\"middle\">
            <div class=\"bar-container\">
              <div class=\"bar-1\"></div>
            </div>
          </div>
          <div class=\"side right\">
            <div>20</div>
          </div>
        </div>
   </form>
 </div>
    
  
{% endblock content%}
       ", "customer_review.html.twig", "C:\\xampp\\htdocs\\ipd17-project\\templates\\customer_review.html.twig");
    }
}
