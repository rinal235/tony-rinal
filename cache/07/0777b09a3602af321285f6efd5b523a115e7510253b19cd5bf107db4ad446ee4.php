<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* payment.html.twig */
class __TwigTemplate_4eacd0611a945955c15c87c9a279d17bcc40700d0d8a51c8a136d9cf9587f22c extends \Twig\Template
{
    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->blocks = [
            'title' => [$this, 'block_title'],
            'headAdd' => [$this, 'block_headAdd'],
            'content' => [$this, 'block_content'],
        ];
    }

    protected function doGetParent(array $context)
    {
        // line 2
        return "master.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $this->parent = $this->loadTemplate("master.html.twig", "payment.html.twig", 2);
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 4
    public function block_title($context, array $blocks = [])
    {
        echo "Payment";
    }

    // line 5
    public function block_headAdd($context, array $blocks = [])
    {
        // line 6
        echo "    <script>
          function yesnoCheck() {
    if (document.getElementById('yesCheck').checked) {
        document.getElementById('ifYes').style.visibility = 'visible';
    }
    else document.getElementById('ifYes').style.visibility = 'hidden';
}
  </script>

";
    }

    // line 16
    public function block_content($context, array $blocks = [])
    {
        // line 17
        echo "    
    ";
        // line 18
        if ((isset($context["errorList"]) ? $context["errorList"] : null)) {
            // line 19
            echo "        <ul class=\"errorMessage\">
            ";
            // line 20
            $context['_parent'] = $context;
            $context['_seq'] = twig_ensure_traversable((isset($context["errorList"]) ? $context["errorList"] : null));
            foreach ($context['_seq'] as $context["_key"] => $context["error"]) {
                // line 21
                echo "                <li>";
                echo twig_escape_filter($this->env, $context["error"], "html", null, true);
                echo "</li>
            ";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['error'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 23
            echo "        </ul>
";
        }
        // line 25
        echo "     <!--display:none //to hide
     display:block //to show-->
   <div id=\"centerContent\">
\t
       
        
        <form method=\"post\" id=\"form\">
            <h2>Payment</h2><br><br>
            
        <div class=\"form-check form-check-inline\">
         Card : <input type=\"radio\"  name=\"rdpayment\"  onclick=\"javascript:yesnoCheck();\" id=\"yesCheck\" >
         Cash : <input type=\"radio\"  checked=\"checked\" name=\"rdpayment\" onclick=\"javascript:yesnoCheck();\" id=\"noCheck\" >
        </div>
            <br><br>
            <div id=\"ifYes\" style=\"visibility:hidden\">
                <br>
            <label for=\"cname\">Name on Card</label>
            <input type=\"text\" id=\"cname\" name=\"cardname\" placeholder=\"John More Doe\"><br><br>
            <label for=\"ccnum\">Credit card number</label>
            <input type=\"text\" id=\"ccnum\" name=\"cardnumber\" placeholder=\"1111-2222-3333-4444\"><br><br>
            <label for=\"expmonth\">Exp Month</label>
            <input type=\"text\" id=\"expmonth\" name=\"expmonth\" placeholder=\"September\"><br><br>
            <label for=\"expyear\">Exp Year</label>
            <input type=\"text\" id=\"expyear\" name=\"expyear\" placeholder=\"2018\"><br><br>
            <label for=\"cvv\">CVV</label>
                <input type=\"text\" id=\"cvv\" name=\"cvv\" placeholder=\"352\">
              </div>
              
            <input type=\"hidden\" name=\"pizzaId\" value=\"";
        // line 53
        echo twig_escape_filter($this->env, (isset($context["pizzaId"]) ? $context["pizzaId"] : null), "html", null, true);
        echo "\"><br>
        <input type=\"submit\" value=\"payment\">
        <br><br>
    Click <a href=\"/pizza_creation\"> here </a> to continue.<br> OR <br>
    Click <a href=\"/logout\"> here </a> to logout.</p>
 
     </form>
        </div>
       ";
    }

    public function getTemplateName()
    {
        return "payment.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  121 => 53,  91 => 25,  87 => 23,  78 => 21,  74 => 20,  71 => 19,  69 => 18,  66 => 17,  63 => 16,  50 => 6,  47 => 5,  41 => 4,  31 => 2,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Source("{# empty Twig template #}
{% extends \"master.html.twig\" %}

{% block title %}Payment{% endblock %}
{% block headAdd %}
    <script>
          function yesnoCheck() {
    if (document.getElementById('yesCheck').checked) {
        document.getElementById('ifYes').style.visibility = 'visible';
    }
    else document.getElementById('ifYes').style.visibility = 'hidden';
}
  </script>

{% endblock %}
{% block content %}
    
    {% if errorList %}
        <ul class=\"errorMessage\">
            {% for error in errorList %}
                <li>{{error}}</li>
            {% endfor %}
        </ul>
{% endif %}
     <!--display:none //to hide
     display:block //to show-->
   <div id=\"centerContent\">
\t
       
        
        <form method=\"post\" id=\"form\">
            <h2>Payment</h2><br><br>
            
        <div class=\"form-check form-check-inline\">
         Card : <input type=\"radio\"  name=\"rdpayment\"  onclick=\"javascript:yesnoCheck();\" id=\"yesCheck\" >
         Cash : <input type=\"radio\"  checked=\"checked\" name=\"rdpayment\" onclick=\"javascript:yesnoCheck();\" id=\"noCheck\" >
        </div>
            <br><br>
            <div id=\"ifYes\" style=\"visibility:hidden\">
                <br>
            <label for=\"cname\">Name on Card</label>
            <input type=\"text\" id=\"cname\" name=\"cardname\" placeholder=\"John More Doe\"><br><br>
            <label for=\"ccnum\">Credit card number</label>
            <input type=\"text\" id=\"ccnum\" name=\"cardnumber\" placeholder=\"1111-2222-3333-4444\"><br><br>
            <label for=\"expmonth\">Exp Month</label>
            <input type=\"text\" id=\"expmonth\" name=\"expmonth\" placeholder=\"September\"><br><br>
            <label for=\"expyear\">Exp Year</label>
            <input type=\"text\" id=\"expyear\" name=\"expyear\" placeholder=\"2018\"><br><br>
            <label for=\"cvv\">CVV</label>
                <input type=\"text\" id=\"cvv\" name=\"cvv\" placeholder=\"352\">
              </div>
              
            <input type=\"hidden\" name=\"pizzaId\" value=\"{{pizzaId}}\"><br>
        <input type=\"submit\" value=\"payment\">
        <br><br>
    Click <a href=\"/pizza_creation\"> here </a> to continue.<br> OR <br>
    Click <a href=\"/logout\"> here </a> to logout.</p>
 
     </form>
        </div>
       {% endblock content%}", "payment.html.twig", "C:\\xampp\\htdocs\\ipd17-project\\templates\\payment.html.twig");
    }
}
