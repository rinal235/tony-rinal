<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* order.html.twig */
class __TwigTemplate_ee2dc0aacd270214720fc545071989bb68015898f7dbefa35ed5601d0ce5cedf extends \Twig\Template
{
    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->blocks = [
            'title' => [$this, 'block_title'],
            'content' => [$this, 'block_content'],
        ];
    }

    protected function doGetParent(array $context)
    {
        // line 1
        return "master.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $this->parent = $this->loadTemplate("master.html.twig", "order.html.twig", 1);
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 3
    public function block_title($context, array $blocks = [])
    {
        echo "Placing order";
    }

    // line 5
    public function block_content($context, array $blocks = [])
    {
        // line 6
        echo "    
    ";
        // line 7
        if (($context["errorList"] ?? null)) {
            // line 8
            echo "        <ul class=\"errorMessage\">
            ";
            // line 9
            $context['_parent'] = $context;
            $context['_seq'] = twig_ensure_traversable(($context["errorList"] ?? null));
            foreach ($context['_seq'] as $context["_key"] => $context["error"]) {
                // line 10
                echo "                <li>";
                echo twig_escape_filter($this->env, $context["error"], "html", null, true);
                echo "</li>
            ";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['error'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 12
            echo "        </ul>
";
        }
        // line 14
        echo "    
   <div id=\"centerContent\">
\t<p>Total value before tax is ";
        // line 16
        echo twig_escape_filter($this->env, ($context["subTotal"] ?? null), "html", null, true);
        echo ".</p>
    
    <p>Taxes (GST/QST/HST) ";
        // line 18
        echo twig_escape_filter($this->env, ($context["tax"] ?? null), "html", null, true);
        echo ". </p>
    <p>Final total with taxes (amount to be paid):
        <b>";
        // line 20
        echo twig_escape_filter($this->env, ($context["total"] ?? null), "html", null, true);
        echo "</b>.</p>
    <form method=\"POST\" id=\"form\" class=\"textbox\">
        Name: <input type=\"text\" name=\"name\" value=\"";
        // line 22
        echo twig_escape_filter($this->env, $this->getAttribute(($context["sessionUser"] ?? null), "name", []), "html", null, true);
        echo "\"><br><br>
        Address:<input type=\"text\" name=\"address\"><br><br>
\tEmail: <input type=\"email\" name=\"email\" value=\"";
        // line 24
        echo twig_escape_filter($this->env, $this->getAttribute(($context["sessionUser"] ?? null), "email", []), "html", null, true);
        echo "\"><br><br>
        Postal code:<input type=\"text\" name=\"postalCode\"><br><br>
        Contact number:<input type=\"text\" name=\"phoneNumber\"><br><br>
\tShipped on:\t<input type=\"date\" name=\"shippedOn\"><br><br>
\tRecieved on:\t<input type=\"date\" name=\"recievedOn\"><br><br>
        Distance: \t<input type=\"text\" name=\"distance\"><br><br><br>
        <input type=\"submit\" value=\"Payment\">
\t
    </form>
   </div>
";
    }

    public function getTemplateName()
    {
        return "order.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  98 => 24,  93 => 22,  88 => 20,  83 => 18,  78 => 16,  74 => 14,  70 => 12,  61 => 10,  57 => 9,  54 => 8,  52 => 7,  49 => 6,  46 => 5,  40 => 3,  30 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Source("{% extends \"master.html.twig\" %}

{% block title %}Placing order{% endblock %}

{% block content %}
    
    {% if errorList %}
        <ul class=\"errorMessage\">
            {% for error in errorList %}
                <li>{{error}}</li>
            {% endfor %}
        </ul>
{% endif %}
    
   <div id=\"centerContent\">
\t<p>Total value before tax is {{ subTotal }}.</p>
    
    <p>Taxes (GST/QST/HST) {{ tax }}. </p>
    <p>Final total with taxes (amount to be paid):
        <b>{{ total }}</b>.</p>
    <form method=\"POST\" id=\"form\" class=\"textbox\">
        Name: <input type=\"text\" name=\"name\" value=\"{{sessionUser.name}}\"><br><br>
        Address:<input type=\"text\" name=\"address\"><br><br>
\tEmail: <input type=\"email\" name=\"email\" value=\"{{sessionUser.email}}\"><br><br>
        Postal code:<input type=\"text\" name=\"postalCode\"><br><br>
        Contact number:<input type=\"text\" name=\"phoneNumber\"><br><br>
\tShipped on:\t<input type=\"date\" name=\"shippedOn\"><br><br>
\tRecieved on:\t<input type=\"date\" name=\"recievedOn\"><br><br>
        Distance: \t<input type=\"text\" name=\"distance\"><br><br><br>
        <input type=\"submit\" value=\"Payment\">
\t
    </form>
   </div>
{% endblock content %}", "order.html.twig", "C:\\xampp\\htdocs\\ipd17-project\\templates\\order.html.twig");
    }
}
