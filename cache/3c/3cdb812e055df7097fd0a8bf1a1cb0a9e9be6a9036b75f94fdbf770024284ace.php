<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* confirm_order.html.twig */
class __TwigTemplate_40b20cd8b4dd1dbc2cc41c015a29d00d41208b2c45cc28543f75b3c52be8a1f8 extends \Twig\Template
{
    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->blocks = [
            'title' => [$this, 'block_title'],
            'content' => [$this, 'block_content'],
        ];
    }

    protected function doGetParent(array $context)
    {
        // line 1
        return "master.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $this->parent = $this->loadTemplate("master.html.twig", "confirm_order.html.twig", 1);
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 2
    public function block_title($context, array $blocks = [])
    {
        echo "Confirm Order Page";
    }

    // line 4
    public function block_content($context, array $blocks = [])
    {
        // line 5
        echo "     <div id=\"centerContent\">
        <form method=\"get\" id=\"form\" action=\"/order\">
    <h2>Confirm You're Order </h2> <br><br>
    <br><br>
    <p>Pizza Size   :  ";
        // line 9
        echo twig_escape_filter($this->env, $this->getAttribute(($context["pizza"] ?? null), "size", []), "html", null, true);
        echo " : ";
        echo twig_escape_filter($this->env, $this->getAttribute(($context["pizza"] ?? null), "sizePrice", []), "html", null, true);
        echo " </p>
    <p>Pizza Crust  :  ";
        // line 10
        echo twig_escape_filter($this->env, $this->getAttribute(($context["pizza"] ?? null), "crust", []), "html", null, true);
        echo " : ";
        echo twig_escape_filter($this->env, $this->getAttribute(($context["pizza"] ?? null), "crustPrice", []), "html", null, true);
        echo "</p>
    <p>Pizza Sauce  :  ";
        // line 11
        echo twig_escape_filter($this->env, $this->getAttribute(($context["pizza"] ?? null), "sauce", []), "html", null, true);
        echo " : ";
        echo twig_escape_filter($this->env, $this->getAttribute(($context["pizza"] ?? null), "saucePrice", []), "html", null, true);
        echo "</p>
    <p>Pepperoni    :  ";
        // line 12
        echo twig_escape_filter($this->env, $this->getAttribute(($context["pizza"] ?? null), "pepperoni", []), "html", null, true);
        echo " : ";
        echo twig_escape_filter($this->env, $this->getAttribute(($context["pizza"] ?? null), "pepperoniPrice", []), "html", null, true);
        echo "</p>
    <p>Cheese       :  ";
        // line 13
        echo twig_escape_filter($this->env, $this->getAttribute(($context["pizza"] ?? null), "cheese", []), "html", null, true);
        echo " : ";
        echo twig_escape_filter($this->env, $this->getAttribute(($context["pizza"] ?? null), "cheesePrice", []), "html", null, true);
        echo "</p>
    <p>Mashrooms    :  ";
        // line 14
        echo twig_escape_filter($this->env, $this->getAttribute(($context["pizza"] ?? null), "mushrooms", []), "html", null, true);
        echo " : ";
        echo twig_escape_filter($this->env, $this->getAttribute(($context["pizza"] ?? null), "mushroomsPrice", []), "html", null, true);
        echo "</p>
    <p>GreenPeppers :  ";
        // line 15
        echo twig_escape_filter($this->env, $this->getAttribute(($context["pizza"] ?? null), "greenPeppers", []), "html", null, true);
        echo ": ";
        echo twig_escape_filter($this->env, $this->getAttribute(($context["pizza"] ?? null), "greenPeppersPrice", []), "html", null, true);
        echo "</p>
    <p>Bacon        :  ";
        // line 16
        echo twig_escape_filter($this->env, $this->getAttribute(($context["pizza"] ?? null), "bacon", []), "html", null, true);
        echo " : ";
        echo twig_escape_filter($this->env, $this->getAttribute(($context["pizza"] ?? null), "baconPrice", []), "html", null, true);
        echo "</p>
    <p>Sausage      :  ";
        // line 17
        echo twig_escape_filter($this->env, $this->getAttribute(($context["pizza"] ?? null), "sausage", []), "html", null, true);
        echo " : ";
        echo twig_escape_filter($this->env, $this->getAttribute(($context["pizza"] ?? null), "sausagePrice", []), "html", null, true);
        echo "</p>
    <p>Ham          :  ";
        // line 18
        echo twig_escape_filter($this->env, $this->getAttribute(($context["pizza"] ?? null), "ham", []), "html", null, true);
        echo " : ";
        echo twig_escape_filter($this->env, $this->getAttribute(($context["pizza"] ?? null), "hamPrice", []), "html", null, true);
        echo "</p>
    <p>Onions       :  ";
        // line 19
        echo twig_escape_filter($this->env, $this->getAttribute(($context["pizza"] ?? null), "onions", []), "html", null, true);
        echo " : ";
        echo twig_escape_filter($this->env, $this->getAttribute(($context["pizza"] ?? null), "onionPrice", []), "html", null, true);
        echo "</p>
    <p>subTotal     :  ";
        // line 20
        echo twig_escape_filter($this->env, $this->getAttribute(($context["pizza"] ?? null), "subtotal", []), "html", null, true);
        echo " </p>
    |<p> gst/qst    :  ";
        // line 21
        echo twig_escape_filter($this->env, $this->getAttribute(($context["pizza"] ?? null), "tax", []), "html", null, true);
        echo "</p>
     <p>Total       :  ";
        // line 22
        echo twig_escape_filter($this->env, $this->getAttribute(($context["pizza"] ?? null), "total", []), "html", null, true);
        echo " </p>
     <p>PizzaId      :  ";
        // line 23
        echo twig_escape_filter($this->env, $this->getAttribute(($context["pizza"] ?? null), "pizzaId", []), "html", null, true);
        echo " </p>
        

     <br><button type=\"submit\" name=\"button\" value=\"";
        // line 26
        echo twig_escape_filter($this->env, $this->getAttribute(($context["pizza"] ?? null), "pizzaId", []), "html", null, true);
        echo "\">Order </button>

    <br>
    <br>
    Click <a href=\"/\"> here </a> to continue.
 </div> 
</form>


";
    }

    public function getTemplateName()
    {
        return "confirm_order.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  139 => 26,  133 => 23,  129 => 22,  125 => 21,  121 => 20,  115 => 19,  109 => 18,  103 => 17,  97 => 16,  91 => 15,  85 => 14,  79 => 13,  73 => 12,  67 => 11,  61 => 10,  55 => 9,  49 => 5,  46 => 4,  40 => 2,  30 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Source("{% extends \"master.html.twig\" %}
{% block title %}Confirm Order Page{% endblock %}

{% block content %}
     <div id=\"centerContent\">
        <form method=\"get\" id=\"form\" action=\"/order\">
    <h2>Confirm You're Order </h2> <br><br>
    <br><br>
    <p>Pizza Size   :  {{pizza.size}} : {{pizza.sizePrice}} </p>
    <p>Pizza Crust  :  {{pizza.crust}} : {{pizza.crustPrice}}</p>
    <p>Pizza Sauce  :  {{pizza.sauce}} : {{pizza.saucePrice}}</p>
    <p>Pepperoni    :  {{pizza.pepperoni}} : {{pizza.pepperoniPrice}}</p>
    <p>Cheese       :  {{pizza.cheese}} : {{pizza. cheesePrice}}</p>
    <p>Mashrooms    :  {{pizza.mushrooms}} : {{pizza.mushroomsPrice}}</p>
    <p>GreenPeppers :  {{pizza.greenPeppers}}: {{pizza.greenPeppersPrice}}</p>
    <p>Bacon        :  {{pizza.bacon}} : {{pizza.baconPrice}}</p>
    <p>Sausage      :  {{pizza.sausage}} : {{pizza.sausagePrice}}</p>
    <p>Ham          :  {{pizza.ham}} : {{pizza.hamPrice}}</p>
    <p>Onions       :  {{pizza.onions}} : {{pizza.onionPrice}}</p>
    <p>subTotal     :  {{pizza.subtotal}} </p>
    |<p> gst/qst    :  {{pizza.tax}}</p>
     <p>Total       :  {{pizza.total}} </p>
     <p>PizzaId      :  {{pizza.pizzaId}} </p>
        

     <br><button type=\"submit\" name=\"button\" value=\"{{pizza.pizzaId}}\">Order </button>

    <br>
    <br>
    Click <a href=\"/\"> here </a> to continue.
 </div> 
</form>


{% endblock %}



", "confirm_order.html.twig", "C:\\xampp\\htdocs\\ipd17-project\\templates\\confirm_order.html.twig");
    }
}
