<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* login_success.html.twig */
class __TwigTemplate_1fbe06843e224e21430951bac09985cbfdb748ece7fa77ac6ce370446d9cf623 extends \Twig\Template
{
    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->blocks = [
            'content' => [$this, 'block_content'],
        ];
    }

    protected function doGetParent(array $context)
    {
        // line 1
        return "master.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $this->parent = $this->loadTemplate("master.html.twig", "login_success.html.twig", 1);
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 2
    public function block_content($context, array $blocks = [])
    {
        // line 3
        echo "    <div id=\"centerContent\">
<form method=\"post\">
    
    <p> You're Logged in Successfully  as ";
        // line 6
        echo twig_escape_filter($this->env, ($context["name"] ?? null), "html", null, true);
        echo "... <br><br>
    Click <a href=\"/pizza_creation\"> here </a> to continue.<br> OR <br>
     Click <a href=\"/logout\"> here </a> to logout.</p>
    </form>
  </div>
";
    }

    public function getTemplateName()
    {
        return "login_success.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  47 => 6,  42 => 3,  39 => 2,  29 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Source("{% extends \"master.html.twig\" %}
{% block content %}
    <div id=\"centerContent\">
<form method=\"post\">
    
    <p> You're Logged in Successfully  as {{name}}... <br><br>
    Click <a href=\"/pizza_creation\"> here </a> to continue.<br> OR <br>
     Click <a href=\"/logout\"> here </a> to logout.</p>
    </form>
  </div>
{% endblock content%}
", "login_success.html.twig", "C:\\xampp\\htdocs\\ipd17-project\\templates\\login_success.html.twig");
    }
}
