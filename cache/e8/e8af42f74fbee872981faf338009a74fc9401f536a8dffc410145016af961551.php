<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* register.html.twig */
class __TwigTemplate_671228269298934ab68fc56459605862897866d3fbb6e01148e984cde66a0000 extends \Twig\Template
{
    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->blocks = [
            'headAdd' => [$this, 'block_headAdd'],
            'title' => [$this, 'block_title'],
            'content' => [$this, 'block_content'],
        ];
    }

    protected function doGetParent(array $context)
    {
        // line 1
        return "master.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $this->parent = $this->loadTemplate("master.html.twig", "register.html.twig", 1);
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 3
    public function block_headAdd($context, array $blocks = [])
    {
        // line 4
        echo " <script>
            \$(document).ready(function() {
                \$('input[name=email]').keyup(function() {
                    var email = \$('input[name=email').val();
                    \$('#isTaken').load(\"/isemailregistered/\" + email);
                });
            });
        </script>
 ";
    }

    // line 14
    public function block_title($context, array $blocks = [])
    {
        echo "User Registration Page";
    }

    // line 16
    public function block_content($context, array $blocks = [])
    {
        // line 17
        echo "    ";
        if (($context["errorList"] ?? null)) {
            // line 18
            echo "        <ul class=\"errorMessage\">
            ";
            // line 19
            $context['_parent'] = $context;
            $context['_seq'] = twig_ensure_traversable(($context["errorList"] ?? null));
            foreach ($context['_seq'] as $context["_key"] => $context["error"]) {
                // line 20
                echo "                <li>";
                echo twig_escape_filter($this->env, $context["error"], "html", null, true);
                echo "</li>
            ";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['error'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 22
            echo "        </ul>
";
        }
        // line 24
        echo "  <div id=\"centerContent\">
<form method=\"post\" id=\"form\" class=\"textbox\">
    
    <input class=\"textbox\" type=\"text\" name=\"name\" value=\"";
        // line 27
        echo twig_escape_filter($this->env, $this->getAttribute(($context["v"] ?? null), "name", []), "html", null, true);
        echo "\" placeholder=\"Name\"><br><br>
    <input class=\"textbox\" type=\"text\" name=\"email\" value=\"";
        // line 28
        echo twig_escape_filter($this->env, $this->getAttribute(($context["v"] ?? null), "email", []), "html", null, true);
        echo "\" placeholder=\"Email\"> <span class=\"errorMessage\" id=\"isTaken\"></span><br><br>  
    <input class=\"textbox\" type=\"password\" name=\"pass1\" placeholder=\"Password\"><br><br>
    <input class=\"textbox\" type=\"password\" name=\"pass2\" placeholder=\"Confirm Password\"><br><br>
    <input class=\"textbox\" type=\"text\" name=\"address\" value=\"";
        // line 31
        echo twig_escape_filter($this->env, $this->getAttribute(($context["v"] ?? null), "address", []), "html", null, true);
        echo "\" placeholder=\"Address\"><br><br>
    <input class=\"textbox\" type=\"text\" name=\"phone\" value=\"";
        // line 32
        echo twig_escape_filter($this->env, $this->getAttribute(($context["v"] ?? null), "phone", []), "html", null, true);
        echo "\" placeholder=\"Phone\"><br><br>
    <input class=\"textbox\" type=\"submit\" value=\"Register\">
</form>
  </div>
";
    }

    public function getTemplateName()
    {
        return "register.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  107 => 32,  103 => 31,  97 => 28,  93 => 27,  88 => 24,  84 => 22,  75 => 20,  71 => 19,  68 => 18,  65 => 17,  62 => 16,  56 => 14,  44 => 4,  41 => 3,  31 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Source("{% extends \"master.html.twig\" %}

{% block headAdd %}
 <script>
            \$(document).ready(function() {
                \$('input[name=email]').keyup(function() {
                    var email = \$('input[name=email').val();
                    \$('#isTaken').load(\"/isemailregistered/\" + email);
                });
            });
        </script>
 {% endblock %}
 
 {% block title %}User Registration Page{% endblock title %}
 
{% block content %}
    {% if errorList %}
        <ul class=\"errorMessage\">
            {% for error in errorList %}
                <li>{{error}}</li>
            {% endfor %}
        </ul>
{% endif %}
  <div id=\"centerContent\">
<form method=\"post\" id=\"form\" class=\"textbox\">
    
    <input class=\"textbox\" type=\"text\" name=\"name\" value=\"{{v.name}}\" placeholder=\"Name\"><br><br>
    <input class=\"textbox\" type=\"text\" name=\"email\" value=\"{{v.email}}\" placeholder=\"Email\"> <span class=\"errorMessage\" id=\"isTaken\"></span><br><br>  
    <input class=\"textbox\" type=\"password\" name=\"pass1\" placeholder=\"Password\"><br><br>
    <input class=\"textbox\" type=\"password\" name=\"pass2\" placeholder=\"Confirm Password\"><br><br>
    <input class=\"textbox\" type=\"text\" name=\"address\" value=\"{{v.address}}\" placeholder=\"Address\"><br><br>
    <input class=\"textbox\" type=\"text\" name=\"phone\" value=\"{{v.phone}}\" placeholder=\"Phone\"><br><br>
    <input class=\"textbox\" type=\"submit\" value=\"Register\">
</form>
  </div>
{% endblock content%}

", "register.html.twig", "C:\\xampp\\htdocs\\ipd17-project\\templates\\register.html.twig");
    }
}
