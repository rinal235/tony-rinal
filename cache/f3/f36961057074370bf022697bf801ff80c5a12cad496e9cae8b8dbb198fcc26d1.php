<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* contact.html.twig */
class __TwigTemplate_4b99651f9f8de86835b449f01fd172e3163d5e45e5b931ef5cd61511020c60b8 extends \Twig\Template
{
    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->blocks = [
            'headAdd' => [$this, 'block_headAdd'],
            'title' => [$this, 'block_title'],
            'content' => [$this, 'block_content'],
        ];
    }

    protected function doGetParent(array $context)
    {
        // line 1
        return "master.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $this->parent = $this->loadTemplate("master.html.twig", "contact.html.twig", 1);
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 2
    public function block_headAdd($context, array $blocks = [])
    {
        // line 3
        echo " 
 ";
    }

    // line 6
    public function block_title($context, array $blocks = [])
    {
        echo "contact Info";
    }

    // line 8
    public function block_content($context, array $blocks = [])
    {
        // line 9
        echo "
<div>
    
    <form>
<!--Grid row-->
<div class=\"row\">

  <!--Grid column-->
  <div class=\"col-md-6 mb-4\">

    <!--Card-->
    <div class=\"card card-cascade narrower\">

      <!--Card image-->
      <div class=\"view view-cascade gradient-card-header blue-gradient\">
        <h5 class=\"mb-0\">Regular map</h5>
      </div>
      <!--/Card image-->

      <!--Card content-->
      <div class=\"card-body card-body-cascade text-center\">

        <!--Google map-->
        <div id=\"map-container-google-8\" class=\"z-depth-1-half map-container-5\" style=\"height: 300px\">
          <iframe src=\"https://maps.google.com/maps?q=laval%20canada&t=&z=13&ie=UTF8&iwloc=&output=embed\"
            frameborder=\"0\" style=\"border:0\" allowfullscreen></iframe>
        </div>

      </div>
      <!--/.Card content-->

    </div>
    <!--/.Card-->

  </div>
  <!--Grid column-->

  <!--Grid column-->
  <div class=\"col-md-6 mb-4\">

    <!--Card-->
    <div class=\"card card-cascade narrower\">

      <!--Card image-->
      <div class=\"view view-cascade gradient-card-header peach-gradient\">
        <h5 class=\"mb-0\">Custom map</h5>
      </div>
      <!--/Card image-->

      <!--Card content-->
      <div class=\"card-body card-body-cascade text-center\">

        <!--Google map-->
        <div id=\"map-container-google-9\" class=\"z-depth-1-half map-container-5\" style=\"height: 300px\">
          <iframe src=\"https://maps.google.com/maps?q=montreal%20canada&t=&z=13&ie=UTF8&iwloc=&output=embed\" frameborder=\"0\"
            style=\"border:0\" allowfullscreen></iframe>
        </div>


      </div>
      <!--/.Card content-->

    </div>
    <!--/.Card-->

  </div>
  <!--Grid column-->

</div>
<!--Grid row-->
<a href=\"tel:123-456-7890p123\">CLICK TO CALL</a>
<img class=\"map\" src=\"https://maps.googleapis.com/maps/api/staticmap?center=";
        // line 80
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["item"]) ? $context["item"] : null), "latitude", []), "html", null, true);
        echo ",";
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["item"]) ? $context["item"] : null), "longitude", []), "html", null, true);
        echo "&size=640x250&zoom=15&markers=size:mid%7Ccolor:red%7Clabel:1%7C";
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["item"]) ? $context["item"] : null), "latitude", []), "html", null, true);
        echo ",";
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["item"]) ? $context["item"] : null), "longitude", []), "html", null, true);
        echo "\" />
</div>
<br>
<br>
<br>
<!-- Footer area-->
<footer>
    
 <!-- Footer -->
        <div class=\"fixed-bottom\">
\t<div class=\"row\">
\t\t<div class=\"col-lg-12 col-md-10 mx-auto text-center\">
\t\t\t<p>Copyright &copy;Rinal & Tony</p>
\t\t</div>
        </div>
\t</div>
 </footer>
    </form>
</div>

";
    }

    public function getTemplateName()
    {
        return "contact.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  131 => 80,  58 => 9,  55 => 8,  49 => 6,  44 => 3,  41 => 2,  31 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Source("{% extends \"master.html.twig\" %}
{% block headAdd %}
 
 {% endblock %}
 
 {% block title %}contact Info{% endblock title %}
 
{% block content %}

<div>
    
    <form>
<!--Grid row-->
<div class=\"row\">

  <!--Grid column-->
  <div class=\"col-md-6 mb-4\">

    <!--Card-->
    <div class=\"card card-cascade narrower\">

      <!--Card image-->
      <div class=\"view view-cascade gradient-card-header blue-gradient\">
        <h5 class=\"mb-0\">Regular map</h5>
      </div>
      <!--/Card image-->

      <!--Card content-->
      <div class=\"card-body card-body-cascade text-center\">

        <!--Google map-->
        <div id=\"map-container-google-8\" class=\"z-depth-1-half map-container-5\" style=\"height: 300px\">
          <iframe src=\"https://maps.google.com/maps?q=laval%20canada&t=&z=13&ie=UTF8&iwloc=&output=embed\"
            frameborder=\"0\" style=\"border:0\" allowfullscreen></iframe>
        </div>

      </div>
      <!--/.Card content-->

    </div>
    <!--/.Card-->

  </div>
  <!--Grid column-->

  <!--Grid column-->
  <div class=\"col-md-6 mb-4\">

    <!--Card-->
    <div class=\"card card-cascade narrower\">

      <!--Card image-->
      <div class=\"view view-cascade gradient-card-header peach-gradient\">
        <h5 class=\"mb-0\">Custom map</h5>
      </div>
      <!--/Card image-->

      <!--Card content-->
      <div class=\"card-body card-body-cascade text-center\">

        <!--Google map-->
        <div id=\"map-container-google-9\" class=\"z-depth-1-half map-container-5\" style=\"height: 300px\">
          <iframe src=\"https://maps.google.com/maps?q=montreal%20canada&t=&z=13&ie=UTF8&iwloc=&output=embed\" frameborder=\"0\"
            style=\"border:0\" allowfullscreen></iframe>
        </div>


      </div>
      <!--/.Card content-->

    </div>
    <!--/.Card-->

  </div>
  <!--Grid column-->

</div>
<!--Grid row-->
<a href=\"tel:123-456-7890p123\">CLICK TO CALL</a>
<img class=\"map\" src=\"https://maps.googleapis.com/maps/api/staticmap?center={{ item.latitude }},{{ item.longitude }}&size=640x250&zoom=15&markers=size:mid%7Ccolor:red%7Clabel:1%7C{{ item.latitude }},{{ item.longitude }}\" />
</div>
<br>
<br>
<br>
<!-- Footer area-->
<footer>
    
 <!-- Footer -->
        <div class=\"fixed-bottom\">
\t<div class=\"row\">
\t\t<div class=\"col-lg-12 col-md-10 mx-auto text-center\">
\t\t\t<p>Copyright &copy;Rinal & Tony</p>
\t\t</div>
        </div>
\t</div>
 </footer>
    </form>
</div>

{% endblock content %}", "contact.html.twig", "C:\\xampp\\htdocs\\tony-rinal\\templates\\contact.html.twig");
    }
}
