<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* order.html.twig */
class __TwigTemplate_dd39e83b69b1fbe33e8cc2fdc235f0eb5535b4e5881f89a018668a0eabdd8a40 extends \Twig\Template
{
    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->blocks = [
            'title' => [$this, 'block_title'],
            'headAdd' => [$this, 'block_headAdd'],
            'content' => [$this, 'block_content'],
        ];
    }

    protected function doGetParent(array $context)
    {
        // line 1
        return "master.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $this->parent = $this->loadTemplate("master.html.twig", "order.html.twig", 1);
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 3
    public function block_title($context, array $blocks = [])
    {
        echo "Making Order";
    }

    // line 4
    public function block_headAdd($context, array $blocks = [])
    {
        // line 5
        echo "    <script>
          function yesnoCheck() {
    if (document.getElementById('yesCheck').checked) {
        document.getElementById('ifYes').style.visibility = 'visible';
    }
    else document.getElementById('ifYes').style.visibility = 'hidden';
}
  </script>
  
";
    }

    // line 15
    public function block_content($context, array $blocks = [])
    {
        // line 16
        echo "    
    ";
        // line 17
        if ((isset($context["errorList"]) ? $context["errorList"] : null)) {
            // line 18
            echo "        <ul class=\"errorMessage\">
            ";
            // line 19
            $context['_parent'] = $context;
            $context['_seq'] = twig_ensure_traversable((isset($context["errorList"]) ? $context["errorList"] : null));
            foreach ($context['_seq'] as $context["_key"] => $context["error"]) {
                // line 20
                echo "                <li>";
                echo twig_escape_filter($this->env, $context["error"], "html", null, true);
                echo "</li>
            ";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['error'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 22
            echo "        </ul>
";
        }
        // line 24
        echo "     <!--display:none //to hide
     display:block //to show-->
   <div id=\"centerContent\">
\t
        
  
        <form method=\"post\" id=\"form\">
            <h2>Service Option</h2><br><br>
            
        <div class=\"form-check form-check-inline\">
         Delivery : <input type=\"radio\"  name=\"rdservice\"  onclick=\"javascript:yesnoCheck();\" id=\"yesCheck\" >
      Pickup : <input type=\"radio\"  checked=\"checked\" name=\"rdservice\" onclick=\"javascript:yesnoCheck();\" id=\"noCheck\" >
        </div>
            <br><br>
     <div id=\"centerContent\">
            <br><br>
            Name: <input type=\"text\" name=\"username\" value=\"";
        // line 40
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["v"]) ? $context["v"] : null), "name", []), "html", null, true);
        echo "\"><br><br>
        Contact number:<input type=\"text\" name=\"contactNumber\" value=\"";
        // line 41
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["v"]) ? $context["v"] : null), "contactNumber", []), "html", null, true);
        echo "\"><br><br>
        <div id=\"ifYes\" style=\"visibility:hidden\">
       Email: <input type=\"email\" name=\"email\" value=\"";
        // line 43
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["v"]) ? $context["v"] : null), "email", []), "html", null, true);
        echo "\"><br><br>
        Shipping Address :<input type=\"text\" name=\"shippingAddress\" value=\"";
        // line 44
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["v"]) ? $context["v"] : null), "shippingAddress", []), "html", null, true);
        echo "\"><br><br>
        Postal code:<input type=\"text\" name=\"postalcode\" value=\"";
        // line 45
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["v"]) ? $context["v"] : null), "postalCode", []), "html", null, true);
        echo "\"><br><br>
    </div>
        <input type=\"hidden\" name=\"pizzaId\" value=\"";
        // line 47
        echo twig_escape_filter($this->env, (isset($context["pizzaId"]) ? $context["pizzaId"] : null), "html", null, true);
        echo "\"><br>
        <input type=\"submit\" value=\"payment\">
        </form>
\t</div>
 
 
   </div>
";
    }

    public function getTemplateName()
    {
        return "order.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  131 => 47,  126 => 45,  122 => 44,  118 => 43,  113 => 41,  109 => 40,  91 => 24,  87 => 22,  78 => 20,  74 => 19,  71 => 18,  69 => 17,  66 => 16,  63 => 15,  50 => 5,  47 => 4,  41 => 3,  31 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Source("{% extends \"master.html.twig\" %}

{% block title %}Making Order{% endblock %}
{% block headAdd %}
    <script>
          function yesnoCheck() {
    if (document.getElementById('yesCheck').checked) {
        document.getElementById('ifYes').style.visibility = 'visible';
    }
    else document.getElementById('ifYes').style.visibility = 'hidden';
}
  </script>
  
{% endblock %}
{% block content %}
    
    {% if errorList %}
        <ul class=\"errorMessage\">
            {% for error in errorList %}
                <li>{{error}}</li>
            {% endfor %}
        </ul>
{% endif %}
     <!--display:none //to hide
     display:block //to show-->
   <div id=\"centerContent\">
\t
        
  
        <form method=\"post\" id=\"form\">
            <h2>Service Option</h2><br><br>
            
        <div class=\"form-check form-check-inline\">
         Delivery : <input type=\"radio\"  name=\"rdservice\"  onclick=\"javascript:yesnoCheck();\" id=\"yesCheck\" >
      Pickup : <input type=\"radio\"  checked=\"checked\" name=\"rdservice\" onclick=\"javascript:yesnoCheck();\" id=\"noCheck\" >
        </div>
            <br><br>
     <div id=\"centerContent\">
            <br><br>
            Name: <input type=\"text\" name=\"username\" value=\"{{v.name}}\"><br><br>
        Contact number:<input type=\"text\" name=\"contactNumber\" value=\"{{v.contactNumber}}\"><br><br>
        <div id=\"ifYes\" style=\"visibility:hidden\">
       Email: <input type=\"email\" name=\"email\" value=\"{{v.email}}\"><br><br>
        Shipping Address :<input type=\"text\" name=\"shippingAddress\" value=\"{{v.shippingAddress}}\"><br><br>
        Postal code:<input type=\"text\" name=\"postalcode\" value=\"{{v.postalCode}}\"><br><br>
    </div>
        <input type=\"hidden\" name=\"pizzaId\" value=\"{{pizzaId}}\"><br>
        <input type=\"submit\" value=\"payment\">
        </form>
\t</div>
 
 
   </div>
{% endblock content %}", "order.html.twig", "C:\\xampp\\htdocs\\ipd17-project\\templates\\order.html.twig");
    }
}
