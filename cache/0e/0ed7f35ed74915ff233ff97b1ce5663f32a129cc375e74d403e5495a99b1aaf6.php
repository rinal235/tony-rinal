<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* register_success.html.twig */
class __TwigTemplate_7a1e48841313e31a45f7b37614a8e2b75dc01fbadfdf9e347a21611760d68892 extends \Twig\Template
{
    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->blocks = [
            'content' => [$this, 'block_content'],
            'title' => [$this, 'block_title'],
        ];
    }

    protected function doGetParent(array $context)
    {
        // line 1
        return "master.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $this->parent = $this->loadTemplate("master.html.twig", "register_success.html.twig", 1);
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 2
    public function block_content($context, array $blocks = [])
    {
        // line 3
        echo "     <div id=\"centerContent\">
    <form id=\"form\" >
    <p id=\"centerContent\"> Registered Successfully...<br><br>
     Click <a href=\"/login\"><u> here</u> </a> to continue.</p>
    </form>
         </div>
";
    }

    // line 10
    public function block_title($context, array $blocks = [])
    {
        echo "Registration completed";
    }

    public function getTemplateName()
    {
        return "register_success.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  53 => 10,  43 => 3,  40 => 2,  30 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Source("{% extends \"master.html.twig\" %}
{% block content %}
     <div id=\"centerContent\">
    <form id=\"form\" >
    <p id=\"centerContent\"> Registered Successfully...<br><br>
     Click <a href=\"/login\"><u> here</u> </a> to continue.</p>
    </form>
         </div>
{% endblock %}
{% block title %}Registration completed{% endblock %}", "register_success.html.twig", "C:\\xampp\\htdocs\\ipd17-project\\templates\\register_success.html.twig");
    }
}
