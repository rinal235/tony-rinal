<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* master.html.twig */
class __TwigTemplate_b25f657bebba3ce9a1e09940df558670128dce631c67ab7394b7edabd4b6aced extends \Twig\Template
{
    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = [
            'title' => [$this, 'block_title'],
            'headAdd' => [$this, 'block_headAdd'],
            'content' => [$this, 'block_content'],
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        // line 1
        echo "<!DOCTYPE html>
<html>
    <head>
            <link rel=\"stylesheet\" href=\"https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css\" integrity=\"sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T\" crossorigin=\"anonymous\">
            
            <script src=\"https://code.jquery.com/jquery-3.3.1.slim.min.js\" integrity=\"sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo\" crossorigin=\"anonymous\"></script>
            
            <script src=\"https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js\" integrity=\"sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1\" crossorigin=\"anonymous\"></script>
            
            <script src=\"https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js\" integrity=\"sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM\" crossorigin=\"anonymous\"></script>
            
             <link rel=\"stylesheet\" href=\"/styles.css\" />
              <link rel=\"stylesheet\" href=\"/login.css\" />
        
            <script src=\"https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js\"></script>
            
            
            <title>";
        // line 18
        $this->displayBlock('title', $context, $blocks);
        echo "</title>
            <meta name=\"description\" content=\"Customize Pizza\"/>
            <meta name=\"keywords\" content=\"Pizza,Customize\"/>
            
            
            ";
        // line 23
        $this->displayBlock('headAdd', $context, $blocks);
        // line 25
        echo "    </head>
    
    <body>
                    <!-- Main Menu Area-->
                 <!-- navigation bar-->
\t\t<nav class=\"navbar navbar-expand-lg navbar-light fixed-top py-2\" id=\"mainNav\">
\t\t\t<div class=\"container-fluid\">
\t\t\t\t<a class=\"navbar-brand js-scroll-trigger\" href=\"#page-top\" id=\"name\">Buddy's Pizza</a>
\t\t\t\t\t<button class=\"navbar-toggler navbar-toggler-right\" type=\"button\" data-toggle=\"collapse\" data-target=\"#navbarResponsive\" aria-controls=\"navbarResponsive\" aria-expanded=\"false\" aria-label=\"Toggle navigation\">
\t\t\t\t\t\t<span class=\"navbar-toggler-icon\"></span>
\t\t\t\t\t</button>
\t\t\t<div class=\"collapse navbar-collapse\" id=\"navbarResponsive\">
\t\t\t\t<ul class=\"navbar-nav ml-auto my-2 my-lg-0\">
\t\t\t\t  <li class=\"nav-item\">
\t\t\t\t\t<a class=\"nav-link js-scroll-trigger\" href=\"/register\" >Register</a>
\t\t\t\t  </li>
\t\t\t\t  <li class=\"nav-item\">
\t\t\t\t\t<a class=\"nav-link js-scroll-trigger\" href=\"/login\">Login</a>
\t\t\t\t  </li>
\t\t\t\t  <li class=\"nav-item\">
\t\t\t\t\t<a class=\"nav-link js-scroll-trigger\" href=\"/logout\">Logout</a>
\t\t\t\t  </li>
\t\t\t\t  <li class=\"nav-item\">
\t\t\t\t\t<a class=\"nav-link js-scroll-trigger\" href=\"/contact\">Contact us</a>
\t\t\t\t  </li>
                                   <li class=\"nav-item\">
\t\t\t\t\t<a class=\"nav-link js-scroll-trigger\" href=\"/customer_review\">Write a review</a>
\t\t\t\t  </li>
\t\t\t\t</ul>
\t\t\t</div>
\t\t\t</div>
\t\t</nav>
<!--Carousel-->
<div class=\"container-fluid\">
    
\t\t<div id=\"carouselExampleIndicators\" class=\"carousel slide\" data-ride=\"carousel\" >
\t\t\t<ol class=\"carousel-indicators\">
\t\t\t\t<li data-target=\"#carouselExampleIndicators\" data-slide-to=\"0\" class=\"active\"></li>
\t\t\t\t<li data-target=\"#carouselExampleIndicators\" data-slide-to=\"1\"></li>
\t\t\t\t<li data-target=\"#carouselExampleIndicators\" data-slide-to=\"2\"></li>
\t\t\t\t<li data-target=\"#carouselExampleIndicators\" data-slide-to=\"3\"></li>
\t\t\t</ol>
\t\t\t<div class=\"carousel-inner\" role=\"listbox\">
\t\t\t\t<!--Slide One-->
\t\t\t\t<div class=\"carousel-item active\"  style=\"background-image: url('/images/Pizza4.jpg')\">
\t\t\t\t\t<div class=\"carousel-caption d-none d-md-block\">
\t\t\t\t\t\t<a class=\"display-4 bg-dark \"></a>
\t\t\t\t\t</div>
\t\t\t\t</div>
\t\t\t\t<!--Slide Two-->
\t\t\t\t<div class=\"carousel-item\" style=\"background-image: url('/images/pizza2.jpg')\">
\t\t\t\t\t<div class=\"carousel-caption d-none d-md-block\">
\t\t\t\t\t\t<a class=\"display-4 bg-dark\"></a>
\t\t\t\t\t</div>
\t\t\t\t</div>
\t\t\t\t<!--Slide Three-->
\t\t\t\t<div class=\"carousel-item\"  style=\"background-image: url('/images/pizza2.png')\">
\t\t\t\t\t<div class=\"carousel-caption d-none d-md-block\">
\t\t\t\t\t\t<a class=\"display-4 bg-dark\"></a>
\t\t\t\t\t</div>
\t\t\t\t</div>
\t\t\t\t<!--Slide Four-->
\t\t\t\t<div class=\"carousel-item\" style=\"background-image: url('/images/pizza5.jpg')\">
\t\t\t\t\t<div class=\"carousel-caption d-none d-md-block\">
\t\t\t\t\t\t<a class=\"display-4 bg-dark\"></a>
\t\t\t\t\t</div>
\t\t\t\t</div>
\t\t\t</div>
\t\t\t<a class=\"carousel-control-prev\" href=\"#carouselExampleIndicators\" role=\"button\" data-slide=\"prev\">
\t\t\t\t<span class=\"carousel-control-prev-icon\" aria-hidden=\"true\"></span>
\t\t\t\t<span class=\"sr-only\">Previous</span>
\t\t\t</a>
\t\t\t<a class=\"carousel-control-next\" href=\"#carouselExampleIndicators\" role=\"button\" data-slide=\"next\">
\t\t\t\t<span class=\"carousel-control-next-icon\" aria-hidden=\"true\"></span>
\t\t\t\t<span class=\"sr-only\">Next</span>
\t\t\t</a>
\t\t</div>
\t</div>

 
    ";
        // line 105
        $this->displayBlock('content', $context, $blocks);
        // line 106
        echo "</div>
<br>
<br>
<br>

<footer> 
 <!-- Footer -->
        <div class=\"fixed-bottom\">
\t<div class=\"row\">
\t\t<div class=\"col-lg-12 col-md-10 mx-auto text-center\">
\t\t\t<p>Copyright &copy;Rinal & Tony</p>
\t\t</div>
        </div>
\t</div>
 </footer>
    </body>
</html>";
    }

    // line 18
    public function block_title($context, array $blocks = [])
    {
    }

    // line 23
    public function block_headAdd($context, array $blocks = [])
    {
        // line 24
        echo "            ";
    }

    // line 105
    public function block_content($context, array $blocks = [])
    {
    }

    public function getTemplateName()
    {
        return "master.html.twig";
    }

    public function getDebugInfo()
    {
        return array (  178 => 105,  174 => 24,  171 => 23,  166 => 18,  146 => 106,  144 => 105,  62 => 25,  60 => 23,  52 => 18,  33 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Source("<!DOCTYPE html>
<html>
    <head>
            <link rel=\"stylesheet\" href=\"https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css\" integrity=\"sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T\" crossorigin=\"anonymous\">
            
            <script src=\"https://code.jquery.com/jquery-3.3.1.slim.min.js\" integrity=\"sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo\" crossorigin=\"anonymous\"></script>
            
            <script src=\"https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js\" integrity=\"sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1\" crossorigin=\"anonymous\"></script>
            
            <script src=\"https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js\" integrity=\"sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM\" crossorigin=\"anonymous\"></script>
            
             <link rel=\"stylesheet\" href=\"/styles.css\" />
              <link rel=\"stylesheet\" href=\"/login.css\" />
        
            <script src=\"https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js\"></script>
            
            
            <title>{% block title %}{% endblock %}</title>
            <meta name=\"description\" content=\"Customize Pizza\"/>
            <meta name=\"keywords\" content=\"Pizza,Customize\"/>
            
            
            {% block headAdd %}
            {% endblock %}
    </head>
    
    <body>
                    <!-- Main Menu Area-->
                 <!-- navigation bar-->
\t\t<nav class=\"navbar navbar-expand-lg navbar-light fixed-top py-2\" id=\"mainNav\">
\t\t\t<div class=\"container-fluid\">
\t\t\t\t<a class=\"navbar-brand js-scroll-trigger\" href=\"#page-top\" id=\"name\">Buddy's Pizza</a>
\t\t\t\t\t<button class=\"navbar-toggler navbar-toggler-right\" type=\"button\" data-toggle=\"collapse\" data-target=\"#navbarResponsive\" aria-controls=\"navbarResponsive\" aria-expanded=\"false\" aria-label=\"Toggle navigation\">
\t\t\t\t\t\t<span class=\"navbar-toggler-icon\"></span>
\t\t\t\t\t</button>
\t\t\t<div class=\"collapse navbar-collapse\" id=\"navbarResponsive\">
\t\t\t\t<ul class=\"navbar-nav ml-auto my-2 my-lg-0\">
\t\t\t\t  <li class=\"nav-item\">
\t\t\t\t\t<a class=\"nav-link js-scroll-trigger\" href=\"/register\" >Register</a>
\t\t\t\t  </li>
\t\t\t\t  <li class=\"nav-item\">
\t\t\t\t\t<a class=\"nav-link js-scroll-trigger\" href=\"/login\">Login</a>
\t\t\t\t  </li>
\t\t\t\t  <li class=\"nav-item\">
\t\t\t\t\t<a class=\"nav-link js-scroll-trigger\" href=\"/logout\">Logout</a>
\t\t\t\t  </li>
\t\t\t\t  <li class=\"nav-item\">
\t\t\t\t\t<a class=\"nav-link js-scroll-trigger\" href=\"/contact\">Contact us</a>
\t\t\t\t  </li>
                                   <li class=\"nav-item\">
\t\t\t\t\t<a class=\"nav-link js-scroll-trigger\" href=\"/customer_review\">Write a review</a>
\t\t\t\t  </li>
\t\t\t\t</ul>
\t\t\t</div>
\t\t\t</div>
\t\t</nav>
<!--Carousel-->
<div class=\"container-fluid\">
    
\t\t<div id=\"carouselExampleIndicators\" class=\"carousel slide\" data-ride=\"carousel\" >
\t\t\t<ol class=\"carousel-indicators\">
\t\t\t\t<li data-target=\"#carouselExampleIndicators\" data-slide-to=\"0\" class=\"active\"></li>
\t\t\t\t<li data-target=\"#carouselExampleIndicators\" data-slide-to=\"1\"></li>
\t\t\t\t<li data-target=\"#carouselExampleIndicators\" data-slide-to=\"2\"></li>
\t\t\t\t<li data-target=\"#carouselExampleIndicators\" data-slide-to=\"3\"></li>
\t\t\t</ol>
\t\t\t<div class=\"carousel-inner\" role=\"listbox\">
\t\t\t\t<!--Slide One-->
\t\t\t\t<div class=\"carousel-item active\"  style=\"background-image: url('/images/Pizza4.jpg')\">
\t\t\t\t\t<div class=\"carousel-caption d-none d-md-block\">
\t\t\t\t\t\t<a class=\"display-4 bg-dark \"></a>
\t\t\t\t\t</div>
\t\t\t\t</div>
\t\t\t\t<!--Slide Two-->
\t\t\t\t<div class=\"carousel-item\" style=\"background-image: url('/images/pizza2.jpg')\">
\t\t\t\t\t<div class=\"carousel-caption d-none d-md-block\">
\t\t\t\t\t\t<a class=\"display-4 bg-dark\"></a>
\t\t\t\t\t</div>
\t\t\t\t</div>
\t\t\t\t<!--Slide Three-->
\t\t\t\t<div class=\"carousel-item\"  style=\"background-image: url('/images/pizza2.png')\">
\t\t\t\t\t<div class=\"carousel-caption d-none d-md-block\">
\t\t\t\t\t\t<a class=\"display-4 bg-dark\"></a>
\t\t\t\t\t</div>
\t\t\t\t</div>
\t\t\t\t<!--Slide Four-->
\t\t\t\t<div class=\"carousel-item\" style=\"background-image: url('/images/pizza5.jpg')\">
\t\t\t\t\t<div class=\"carousel-caption d-none d-md-block\">
\t\t\t\t\t\t<a class=\"display-4 bg-dark\"></a>
\t\t\t\t\t</div>
\t\t\t\t</div>
\t\t\t</div>
\t\t\t<a class=\"carousel-control-prev\" href=\"#carouselExampleIndicators\" role=\"button\" data-slide=\"prev\">
\t\t\t\t<span class=\"carousel-control-prev-icon\" aria-hidden=\"true\"></span>
\t\t\t\t<span class=\"sr-only\">Previous</span>
\t\t\t</a>
\t\t\t<a class=\"carousel-control-next\" href=\"#carouselExampleIndicators\" role=\"button\" data-slide=\"next\">
\t\t\t\t<span class=\"carousel-control-next-icon\" aria-hidden=\"true\"></span>
\t\t\t\t<span class=\"sr-only\">Next</span>
\t\t\t</a>
\t\t</div>
\t</div>

 
    {% block content %}{% endblock %}
</div>
<br>
<br>
<br>

<footer> 
 <!-- Footer -->
        <div class=\"fixed-bottom\">
\t<div class=\"row\">
\t\t<div class=\"col-lg-12 col-md-10 mx-auto text-center\">
\t\t\t<p>Copyright &copy;Rinal & Tony</p>
\t\t</div>
        </div>
\t</div>
 </footer>
    </body>
</html>", "master.html.twig", "C:\\xampp\\htdocs\\ipd17-project\\templates\\master.html.twig");
    }
}
