<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* login.html.twig */
class __TwigTemplate_ee431407c5c48516ba8a3db831fb2783e485a04a451dece234391d269f83036d extends \Twig\Template
{
    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->blocks = [
            'title' => [$this, 'block_title'],
            'content' => [$this, 'block_content'],
        ];
    }

    protected function doGetParent(array $context)
    {
        // line 1
        return "master.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $this->parent = $this->loadTemplate("master.html.twig", "login.html.twig", 1);
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 5
    public function block_title($context, array $blocks = [])
    {
        echo "Login";
    }

    // line 7
    public function block_content($context, array $blocks = [])
    {
        // line 8
        echo "    
    ";
        // line 9
        if (($context["error"] ?? null)) {
            // line 10
            echo "        <p class=\"errorMessage\">Invalid login credentials, try again....or <br><br> <a href=\"/register\">register</a></p>
    ";
        }
        // line 12
        echo "    <div id=\"centerContent\">
   
 <form method=\"post\" id=\"form\">
<body>
  <div class=\"container-fluid\">
    <div class=\"row\">
      <div class=\"col-sm-9 col-md-7 col-lg-5 mx-auto\">
        <div class=\"card card-signin my-5\">
          <div class=\"card-body\">
            <h5 class=\"card-title text-center\">Log In</h5>
            <form class=\"form-signin\">
              <div class=\"form-label-group\">
                <input type=\"email\" id=\"inputEmail\" class=\"form-control\" placeholder=\"Email address\" required autofocus name=\"email\" value=\"";
        // line 24
        echo twig_escape_filter($this->env, $this->getAttribute(($context["v"] ?? null), "email", []), "html", null, true);
        echo "\">
                <label for=\"inputEmail\">Email address</label>
              </div>

              <div class=\"form-label-group\">
                <input type=\"password\" id=\"inputPassword\" class=\"form-control\" placeholder=\"Password\" required name=\"password\">
                <label for=\"inputPassword\">Password</label>
              </div>

            
              <button class=\"btn btn-lg btn-primary btn-block text-uppercase\" type=\"submit\">Log in</button>
              <hr class=\"my-4\">
             
            </form>
          </div>
        </div>
      </div>
    </div>
  </div>
</body>
 </form>
    </div>
    ";
    }

    public function getTemplateName()
    {
        return "login.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  72 => 24,  58 => 12,  54 => 10,  52 => 9,  49 => 8,  46 => 7,  40 => 5,  30 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Source("{% extends \"master.html.twig\" %}



 {% block title %}Login{% endblock title %}

{% block content %}
    
    {% if error %}
        <p class=\"errorMessage\">Invalid login credentials, try again....or <br><br> <a href=\"/register\">register</a></p>
    {% endif %}
    <div id=\"centerContent\">
   
 <form method=\"post\" id=\"form\">
<body>
  <div class=\"container-fluid\">
    <div class=\"row\">
      <div class=\"col-sm-9 col-md-7 col-lg-5 mx-auto\">
        <div class=\"card card-signin my-5\">
          <div class=\"card-body\">
            <h5 class=\"card-title text-center\">Log In</h5>
            <form class=\"form-signin\">
              <div class=\"form-label-group\">
                <input type=\"email\" id=\"inputEmail\" class=\"form-control\" placeholder=\"Email address\" required autofocus name=\"email\" value=\"{{v.email}}\">
                <label for=\"inputEmail\">Email address</label>
              </div>

              <div class=\"form-label-group\">
                <input type=\"password\" id=\"inputPassword\" class=\"form-control\" placeholder=\"Password\" required name=\"password\">
                <label for=\"inputPassword\">Password</label>
              </div>

            
              <button class=\"btn btn-lg btn-primary btn-block text-uppercase\" type=\"submit\">Log in</button>
              <hr class=\"my-4\">
             
            </form>
          </div>
        </div>
      </div>
    </div>
  </div>
</body>
 </form>
    </div>
    {% endblock content %}", "login.html.twig", "C:\\xampp\\htdocs\\ipd17-project\\templates\\login.html.twig");
    }
}
