<?php
// changed
//lTdyr4vca3lSOUYM
session_cache_limiter(false);
session_start();

require_once 'vendor/autoload.php';

use Monolog\Logger;
use Monolog\Handler\StreamHandler;

// create a log channel
$log = new Logger('main');
$log->pushHandler(new StreamHandler('logs/everything.log', Logger::DEBUG));
$log->pushHandler(new StreamHandler('logs/errors.log', Logger::ERROR));

/* DB::$user = 'pizza';
  DB::$password = 'EhWkeB7saRNgMWib';
  DB::$dbName = 'pizza';
  DB::$encoding='utf8';
  DB::$port=3333; */

if ($_SERVER['SERVER_NAME'] == 'localhost') {//localhost
    //DB::$user = 'pizzaNew'; // rinal school computer
    //DB::$user = 'pizza'; //tony house computer
    DB::$user = 'pizzaclass'; //tony house computer
    
    //DB::$password = 'Rina2019ipd17'; // localhost class
    //DB::$password = 'KXnQyaoXXyfbM05U'; // localhost class tony
    DB::$password = 'abPGhlS7f1yMsHZL'; // localhost home tony    
   
    //DB::$dbName = 'pizzaNew';  // rinal school computer
    DB::$dbName = 'pizzaclass';  // tony home computer
    DB::$encoding = 'utf8';
    DB::$port = 3333; //3333 class
} else {//ipd17.com
    DB::$user = 'pizza';
    DB::$password = 'fl3aqdk9a369';
    DB::$dbName = 'pizza';
    DB::$encoding = 'utf8';
    //fl3aqdk9a369
}


DB::$error_handler = 'database_error_handler';
DB::$nonsql_error_handler = 'database_error_handler';

function database_error_handler($params) {
    global $app, $log;
    $log->error("SQL Error: " . $params['error']);
    if (isset($params['query'])) {
        $log->error("SQL Query: " . $params['query']);
    }
    $app->render("internal_error.html.twig");
    http_response_code(500);
    die; // don't want to keep going if a query broke
}

// Slim creation and setup
$app = new \Slim\Slim(array(
    'view' => new \Slim\Views\Twig()
        ));

$view = $app->view();
$view->parserOptions = array(
    'debug' => true,
    'cache' => dirname(__FILE__) . '/cache'
);
$view->setTemplatesDirectory(dirname(__FILE__) . '/templates');


\Slim\Route::setDefaultConditions(array(
    'id' => '[1-9][0-9]*'
));

function getUserIpAddr() {
    if (!empty($_SERVER['HTTP_CLIENT_IP'])) {
        //ip from share internet
        $ip = $_SERVER['HTTP_CLIENT_IP'];
    } elseif (!empty($_SERVER['HTTP_X_FORWARDED_FOR'])) {
        //ip pass from proxy
        $ip = $_SERVER['HTTP_X_FORWARDED_FOR'];
    } else {
        $ip = $_SERVER['REMOTE_ADDR'];
    }
    return $ip;
}

//==========================================================================================================================
$app->get('/internalerror', function() use ($app, $log) {
    $app->render("internal_error.html.twig");
});

$app->get('/accessdenied', function() use ($app) {
    $app->render('accessdenied.html.twig');
});

$app->get('/isemailregistered/(:email)', function($email = "") use ($app) {
    $user = DB::queryFirstRow("SELECT * FROM customers WHERE email=%s", $email);
    if ($user) {
        echo "Email already registered";
    }
});

$app->get('/', function() use ($app) {
    echo"dfgdg";
  // $list = DB::query("SELECT * FROM pizza");    //fix me
    $app->render('startingpage.html.twig');            // fixme need index.html.twig file
});
//============================================================================================================================

// TRI-STATE FOR CUSTOMER TABLE
// STATE 1: FIRST SHOW
$app->get('/register', function() use ($app) {
    $app->render('register.html.twig');
});

$app->post('/register', function() use ($app, $log) {
    $name = $app->request()->post('name');
    $email = $app->request()->post('email');
    $pass1 = $app->request()->post('pass1');
    $pass2 = $app->request()->post('pass2');
    $address = $app->request()->post('address');
    $phone = $app->request()->post('phone');
    $errorList = array();
    if (strlen($name) < 2 || strlen($name) > 50) {
        array_push($errorList, "Name must be 2-50 characters long");
        $name = "";
    }

    if (filter_var($email, FILTER_VALIDATE_EMAIL) == FALSE) {
        array_push($errorList, "Email invalid");
        $email = "";
    }
    
   /* $account = DB::queryFirstRow("SELECT * FROM customers WHERE email=%s", $email);
    if ($account) {
        array_push($errorList, "This user is already registered. Please, enter a different email.");
        $email = "";
    }*/

    if ($pass1 != $pass2) {
        array_push($errorList, "Passwords must match");
    } else {
        echo $pass1;
        if ((strlen($pass1) < 6) || (preg_match("/[A-Z]/", $pass1) == FALSE ) || (preg_match("/[a-z]/", $pass1) == FALSE ) || (preg_match("/[0-9]/", $pass1) == FALSE )) {
            array_push($errorList, "Password must be at least 6 characters long, "
                    . "with at least one uppercase, one lowercase, and one digit in it");
        }
    }
    if (strlen($address) < 10 || strlen($address) > 250) {                              // CUSTOMERS.ADDRESS VALIDATION 
        array_push($errorList, "address must between 10-250 characters long");
        $address = "";
    }

    if (preg_match("/^[0-9]{3}-[0-9]{4}-[0-9]{4}$/", $phone)) {          // CUSTOMERS.PHONE VALIDATION
        array_push($errorList, "phone number not valid");
        $phone = "";
    }
    if ($errorList) { // STATE 2: failed submission
        echo "errors";
        $app->render('register.html.twig', array(
            'errorList' => $errorList,
            'v' => array('name' => $name, 'email' => $email, 'address' => $address, 'phone' => $phone)  // NO PASSWORD HERE
        ));
    } else { // STATE 3: successful submission
        echo "success";
        DB::insert('customers', array('name' => $name, 'email' => $email, 'password' => $pass1, 'address' => $address, 'phone' => $phone)); //HERE THERE IS PASSWORD
        $userId = DB::insertId();
        $log->debug("User registed with id=" . $userId);
        $app->render('register_success.html.twig');
    }
});
/* ------------------------------------------------------------------------------------------------------------------- */
// STATE 1: first show                                    // LOGIN VERIFICATION
$app->get('/login', function() use ($app) {
    $app->render('login.html.twig');
});

$app->post('/login', function() use ($app, $log) {
    $email = $app->request()->post('email');
    $password = $app->request()->post('password');
    //
    $loginSuccessful = false;
    $user = DB::queryFirstRow("SELECT * FROM customers WHERE email=%s", $email);
    if ($user) {
        if ($user['password'] == $password) {
            $loginSuccessful = true;
        }
    }
    //
    if (!$loginSuccessful) { // array not empty -> errors present
        $log->info(sprintf("Login failed, email=%s, from IP=%s", $email, getUserIpAddr()));
        $app->render('login.html.twig', array('error' => true));
    } else { // STATE 3: successful submission
        unset($user['password']);
        $_SESSION['user'] = $user;
        $log->info(sprintf("Login successful, email=%s, from IP=%s", $email, getUserIpAddr()));
        $app->render('login_success.html.twig');
    }
});

$app->get('/login_success', function() use ($app) {
    $app->render('login_success.html.twig');
});

$app->get('/logout', function() use ($app) {
    unset($_SESSION['user']);
    $app->render('logout.html.twig');
});
//=============================================================================================================
$app->get('/session', function() {
    echo '<pre>';
    print_r($_SESSION);
});


/* ------------------------------------------------------------------------------------------------------------------ */
// TRI-STATE FOR INGREDIENTS TABLE          //NOT SURE ABOUT THIS YET... HAVE TO SEE IF I DONT RESTRUCTURE THE TABLE THE WAY TEACHER SUGGESTED
// STATE 1: FIRST SHOW   
$app->get('/pizza_creation', function() use ($app) {
    $app->render('pizza_creation.html.twig');
});
$app->post('/pizza_creation', function() use ($app, $log) {
    $size = $app->request()->post('size');
    $crust = $app->request()->post('crust');
    $sauce = $app->request()->post('sauce');
    $pepperoni = $app->request()->post('pepperoni');
    $cheese = $app->request()->post('cheese');
    $mushrooms = $app->request()->post('mushrooms');
    $greenPeppers = $app->request()->post('greenPeppers');
    $bacon = $app->request()->post('bacon');
    $sausage = $app->request()->post('sausage');
    $ham = $app->request()->post('ham');
    $onions = $app->request()->post('onions');
    
    $size = $app->request()->post('rdSize');
    $crust = $app->request()->post('rdCrust');
    $sauce = $app->request()->post('rdsauce');
    $pepperoni = $app->request()->post('rdpepperoni');
    $cheese = $app->request()->post('rdcheese');
    $mushrooms = $app->request()->post('rdmushrooms');
    $greenPeppers = $app->request()->post('rdgreenPeppers');
    $bacon = $app->request()->post('rdbacon');
    $sausage = $app->request()->post('rdsausage');
    $ham = $app->request()->post('rdham');
    $onions = $app->request()->post('rdonions');
    
    $sizePrice=$crustPrice=$saucePrice=$pepperoniPrice=
            $cheesePrice=$mushroomsPrice=$greenPeppersPrice=$baconPrice=
            $sausagePrice=$hamPrice=$onionPrice=0;
            
    if ($size == 'small') {                                       
        $size = 'small';   
        $sizePrice = 4;
    }if ($size == 'medium') {
        $size = 'medium';
        $sizePrice = 6;
    }if ($size == 'large') {
        $size = 'large';
        $sizePrice = 9;
    }if ($size == 'x-large') {
        $size = 'x-large';
        $sizePrice = 13;
    }
    if ($crust == 'thin') {                                             
        $crust = 'thin';
        $crustPrice = 4;
    }if ($crust == 'regular') {
        $crust = 'regular';
        $crustPrice = 5;
    }if ($crust == 'stuffed') {
        $crust = 'stuffed';
        $crustPrice = 6;
    }
   
    if ($sauce == 'pizza') {
        $sauce = 'pizza';
        $saucePrice = 2;
    }if ($sauce == 'bbq') {
        $sauce = 'bbq';
        $saucePrice = 2.5;
    }if ($sauce == 'nosauce') {
        $sauce = 'nosauce';
        $saucePrice = 0;
    }
    if ($pepperoni == 'extrapepperoni'){
        $pepperoni = 'extrapepperoni';
        $pepperoniPrice = 2;
    }if ($pepperoni == 'pepperoni'){
        $pepperoni = 'pepperoni';
        $pepperoniPrice = 1;
    }if ($pepperoni == 'nopepperoni'){  
        $pepperoni = 'nopepperoni';
        $pepperoniPrice = 0;
    }
    
    if ($cheese == 'extramozzarella') {
         $cheese = 'extramozzarella';
        $cheesePrice = 3;
    }if ($cheese == 'mozzarella') {
        $cheese = 'mozzarella';
        $cheesePrice = 2;
    }if ($cheese == 'nomozzarella') {
        $cheese = 'nomozzarella';
        $cheesePrice = 0;
    }
    
    if ($mushrooms == 'extramushrooms') {
        $mushrooms = 'extramushrooms';
        $mushroomsPrice = 1;
    }if ($mushrooms == 'mushrooms') {
        $mushrooms = 'mushrooms';
        $mushroomsPrice = 0.5;
    }if ($mushrooms == 'nomushrooms') {
        $mushrooms = 'nomushrooms';
        $mushroomsPrice = 0;
    }
   
    if ($greenPeppers == 'extragreenPeppers') {                                         
        $greenPeppers = 'extragreenPeppers';
        $greenPeppersPrice = 1.5;
    }if ($greenPeppers == 'greenPeppers') {                                          
        $greenPeppers = 'greenPeppers';
        $greenPeppersPrice = 1;
    }if ($greenPeppers == 'nogreenPeppers') {
        $greenPeppers = 'nogreenPeppers';
        $greenPeppersPrice = 0;
    }
    if ($bacon == 'extrabacon') {                                         
        $bacon = 'extrabacon';
        $baconPrice = 4;
    }if ($bacon == 'bacon') {                                           
        $bacon = 'bacon';
        $baconPrice = 2.5;
    }if ($bacon == 'nobacon') {
        $bacon = 'nobacon';
        $baconPrice = 0;
    }
    if ($sausage == 'extrasausage') {                                           
        $sausage = 'extrasausage';
        $sausagePrice = 4;
    }if ($sausage == 'sausage') {                                          
        $sausage = 'sausage';
        $sausagePrice = 2;
    }if ($sausage == 'nosausage') {
        $sausage = 'nosausage';
        $sausagePrice = 0;
    }
    if ($ham == 'extraham') {                                           
        $ham = 'extraham';
        $hamPrice = 3;
    }if ($ham == 'ham') {                                           
        $ham = 'ham';
        $hamPrice = 2;
    }if ($ham == 'noham') {
        $ham = 'noham';
        $hamPrice = 0;
    }
    if ($onions == 'extraonions') {                                       
        $onions = 'extraonions';
        $onionPrice= 1;
    }if ($onions == 'onions') {                                        
        $onions = 'onions';
        $onionPrice = 0.5;
    }if ($onions == 'noonions') {
        $onions = 'noonions';
        $onionPrice = 0;
    }
    
    $subTotal = $sizePrice+$crustPrice+$saucePrice+$pepperoniPrice+$cheesePrice+$mushroomsPrice
             +$greenPeppersPrice+$baconPrice+$sausagePrice+$hamPrice+$onionPrice;
    $tax = ($subTotal *0.15);
    $total = ($subTotal + $tax);
    //print_r($pizza);
 
    $customerId =$_SESSION['user']['id'] ;
      
    DB::insert('pizzas', array('customerId'=>$customerId, 'size'=>$size, 'crust' => $crust, 'sauce' => $sauce, 'pepperoni' =>$pepperoni, 'cheese' => $cheese, 'mushrooms' => $mushrooms,
                'greenPeppers' => $greenPeppers, 'bacon' => $bacon,
                'sausage' => $sausage, 'ham' => $ham, 'onions' => $onions));
        $pizzaId = DB::insertId();
        $pizza = array('size'=>$size, 'sizePrice'=>$sizePrice,
                       'crust'=>$crust,'crustPrice'=>$crustPrice,
                       'sauce'=>$sauce,'saucePrice'=>$saucePrice,
                       'pepperoni'=>$pepperoni,'pepperoniPrice'=>$pepperoniPrice,
                       'cheese'=>$cheese,'cheesePrice'=>$cheesePrice,
                       'mushrooms'=>$mushrooms,'mushroomsPrice'=>$mushroomsPrice,
                       'greenPeppers'=>$greenPeppers,'greenPeppersPrice'=>$greenPeppersPrice,
                       'bacon'=>$bacon,'baconPrice'=>$baconPrice,
                       'sausage'=>$sausage,'sausagePrice'=>$sausagePrice,
                       'ham'=>$ham,'hamPrice'=>$hamPrice,
                       'onions'=>$onions,'onionPrice'=>$onionPrice,
          
        'size'=>$size,'crust'=>$crust,'rdsauce'=>$sauce,'rdpepperoni'=>$pepperoni,'rdcheese'=>$cheese,
        'rdmushrooms'=>$mushrooms,'rdgreenPeppers'=>$greenPeppers,'rdbacon'=>$bacon,'rdsausage'=>$sausage,
        'rdham'=>$ham,'rdonions'=>$onions, 'subtotal'=>$subTotal,'tax'=>$tax,'total'=>$total,'pizzaId'=>$pizzaId);
        $log->debug("Pizza created with id =" . $pizzaId);
        
        //print_r($pizza);
        $app->render("confirm_order.html.twig",array('pizza'=>$pizza));
});
//====================================================================================================================
// TRI-STATE FOR ORDERS TABLE         
// STATE 1: FIRST SHOW         
$app->get('/order', function() use ($app) {
    $pizzaId = $app->request()->get('button');
     //echo $pizzaId; 
    $app->render('order.html.twig',array('pizzaId'=>$pizzaId ));
});

$app->post('/order', function() use ($app, $log) { 
    $name = $app->request()->post('username');
    $email = $app->request()->post('email');
    $shippingAddress = $app->request()->post('shippingAddress');
    $postalcode =$app->request()->post('postalcode');
    $contactNumber = $app->request()->post('contactNumber');
    $service = $app->request()->post('service');
    $pizzaId = $app->request()->post('pizzaId');
    
     //echo $pizzaId ." ". $name ." ". $email  ." ". $contactNumber ." ".  $service;
    
    $service = $app->request()->post('rdservice');
    if ( $service == 'delivery') {                                       
         $service = 'delivery';   
       
    }if ( $service== 'pickup') {
        $service= 'pickup';
    }
      
    $errorList = array();

    if (strlen($name) < 2 || strlen($name) > 50 || $name = "") {                                  // ORDERS.NAME VALIDATION
        array_push($errorList, "Name must be 2-50 characters long");
        $name = "";
    }
     if (preg_match("/^[0-9]{3}-[0-9]{4}-[0-9]{4}$/", $contactNumber)) {          // CUSTOMERS.PHONE VALIDATION
        array_push($errorList, "phone number not valid");
        $contactNumber = "";
    }   
    
//    if (filter_var($email, FILTER_VALIDATE_EMAIL) == FALSE) {
//        array_push($errorList, "Email invalid");
//        $email = "";
//    }
    /*
    $account = DB::queryFirstRow("SELECT * FROM customers WHERE email=%s", $email);
    if ($account) {
        array_push($errorList, "This user is already registered. Please, enter a different email.");
        $email = "";
    }*/
      
//     if (strlen($shippingAddress) < 10 || strlen($shippingAddress) > 250 ) {                                  // ORDERS.ADDRESS VALIDATION   
//        array_push($errorList, "Delivery address must between 10-250 characters long");
//        $shippingAddress = "";
//    }
   
     //$user= DB::queryFirstRow("SELECT * FROM pizzas WHERE id=%s", $pizzaId); 
     
    if ($errorList) { // STATE 2: failed submission
        $app->render('order.html.twig', array(
            'errorList' => $errorList,
            'v' => array('name' => $name,'email' =>$email,'shippingAddress' => $shippingAddress, $postalcode =>'postalcode', 'contactNumber' => $contactNumber ,
                'service' => $service)   
        ));
    } else { // STATE 3: successful submission
        
       
        DB::insert('orders', array('pizzaId'=>$pizzaId,'name' => $name,'email' =>$email,'shippingAddress' => $shippingAddress,'postalcode'=>$postalcode,
            'contactNumber' => $contactNumber,'service' => $service));
          $pizzaId = DB::insertId();
         //$log->debug("User orders with id=" . $orderId);
        $app->render('payment.html.twig');
    }
});

//=========================================================================================================================
$app->get('/payment', function() use ($app) {
    $app->render('payment.html.twig');
});

/* ======================================================================================================================= */
$app->get('/contact', function() use ($app) {
    $app->render('contact.html.twig');
});

/* ======================================================================================================================= */
// TRI-STATE FOR CUSTOMER_REVIEW TABLE
// STATE 1: FIRST SHOW
$app->get('/customer_review', function() use ($app) {
    $app->render('customer_review.html.twig');
});

$app->post('/customer_review', function() use ($app, $log) {       // CUSTOMER_REVIEW TABLE
    $name = $app->request()->post('name');
    $email = $app->request()->post('email');
    $comment = $app->request()->post('comment');
    $errorList = array();

    if (strlen($name) < 2 || strlen($name) > 50) {                                  // CUSTOMER_REVIEW .NAME VALIDATION
        array_push($errorList, "Name must be 2-50 characters long");
        $name = "";
    }
    if (filter_var($email, FILTER_VALIDATE_EMAIL) == FALSE) {                       // CUSTOMER_REVIEW.EMAIL VALIDATION
        array_push($errorList, "Email invalid");
        $email = "";
    } else {
        // FIXME: Make sure email is not already registered !
    }
    if (strlen($comment) < 5 || strlen($comment) > 65000) {                               //CUSTOMER_REVIEW.REVIEW VALIDATION
        array_push($errorList, "Review must be 5-65000 characters long");
    }
    if ($errorList) { // STATE 2: failed submission
	echo "fail";
        $app->render('customer_review.html.twig', array(
            'errorList' => $errorList,
            'v' => array('name' => $name, 'email' => $email, 'comment' => $comment)
        ));
    } else { // STATE 3: successful submission
		echo "success";
		$qry="SELECT `customers`.id , `reviews`.customer_id FROM `customers` INNER JOIN `reviews` ON `customers`.id = `reviews`.customer_id ORDER by `customer`.id"; //FIXME for inner join
        DB::insert('reviews', array('name' => $name, 'email' => $email, 'comment' => $comment));
        $userId = DB::insertId($qry);
        $log->debug("review added successfully=" . $userId);
        $app->render('customer_review_success.html.twig');
    }
});

//==================================================================================================================
$app->get('/customer_review_success', function() use ($app) {
    $app->render('customer_review_success.html.twig');
});
//=======================================================================================================================

$app->run();
